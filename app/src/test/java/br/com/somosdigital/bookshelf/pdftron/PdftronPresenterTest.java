package br.com.somosdigital.bookshelf.pdftron;

import org.json.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.storage.preferences.LocalPreferences;
import br.com.somosdigital.bookshelf.domain.bookshelf.reader.PdfReaderInteractor;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookFile;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderContract;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderPresenter;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jgsmanaroulas on 01/02/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class PdftronPresenterTest {
//
    private PdfReaderPresenter presenter;

    @Mock
    private PdfReaderContract.View view;

    @Mock
    private PdfReaderInteractor interactor;

    @Mock
    private ArrayList<BookActivityInfo> activities;

    @Mock
    private JSONArray jsonArray;

    @Mock
    private Log log;

    @Mock
    private LocalPreferences localPreferences;

    private HashMap<Integer, List<BookActivityInfo>> activityMap = new HashMap<>();

    private String bookId = "livro";

    @Mock
    private List<OedInfo> mockOedList;

    @Mock
    private BookFile bookFile;

    @Before
    public void setUp(){
        presenter = new PdfReaderPresenter(log, interactor);
        presenter.attachView(view);
    }

    @After
    public void tearDown(){
        if (presenter != null) {
            presenter.detachView();
        }
    }

    @Test
    public void loadValidPDFDocument() throws Exception {
        when(interactor.checkIfDocumentExists()).thenReturn(true);

        presenter.loadPDFDocument(bookId);

        verify(view).onDocumentLoading();
        verify(view).onLocalDocumentReady(anyInt());
    }

    @Test
    public void loadPageActivities() throws Exception {
        int[] currentPage = new int[]{1};

        presenter.loadPageActivities(currentPage, activityMap);

        verify(view).onActivitiesLoaded(any());
    }

    @Test
    public void preloadOedsSuccess() throws Exception {
        when(interactor.getOedList()).thenReturn(mockOedList);
        when(mockOedList.size()).thenReturn(2);

        presenter.preloadOeds();

        verify(view).onOedsLoaded(mockOedList);
    }

    @Test
    public void preloadOedsNoOeds() throws Exception {
        when(interactor.getOedList()).thenReturn(mockOedList);
        when(mockOedList.size()).thenReturn(0);

        presenter.preloadOeds();

        verify(view).onNoOedsLoaded();
    }
}
