package br.com.somosdigital.bookshelf.service.distribuidor;

import java.util.List;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.ActivityFile;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookFile;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Chapter;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class DistribuidorService {

    private final String EMBEDDED = "embedded";

    private DistribuidorServiceInterface distribuidorInterface;

    public DistribuidorService(DistribuidorServiceInterface distribuidorInterface) {
        this.distribuidorInterface = distribuidorInterface;
    }

    public Observable<ActivityFile> getActivityFile(ActivityFile activityObject){
        return distribuidorInterface.getActivityFile(activityObject);
    }


    public Observable<List<Book>> getBooks(String deviceId,
                                           String screenDensity,
                                           String fileType){

        return distribuidorInterface.getBooks(deviceId, screenDensity, fileType);
    }

    public Observable<BookFile> getBookFile(BookFile bookObject){
        return distribuidorInterface.getBookFile(bookObject);
    }

    public Observable<Oed> getOedFile(Oed oedObject){
        return distribuidorInterface.getOedFile(oedObject);
    }

    public Observable<List<Chapter>> getChapters(String bookId){
        String chapterQuery ="_book.id==@";
        String finalQuery = getFinalQuery(chapterQuery, bookId);

        return distribuidorInterface.getChapters(finalQuery, EMBEDDED, "order", "10000");
    }

    public Observable<Response<ResponseBody>> getXFDFFile(String bookId){
        return distribuidorInterface.getXFDFFile(bookId);
    }

    public Observable<Response<ResponseBody>> sendXFDFFile(String bookId, RequestBody body){
        return distribuidorInterface.sendXFDFFile(bookId, body);
    }

    private String getFinalQuery(String endpoint, String... params) {
        for (int i = 0; i < params.length; i++) {
            endpoint = endpoint.replaceFirst("@", params[i]);
        }
        return endpoint;
    }
}
