package br.com.somosdigital.bookshelf.service.template;

import br.com.solucaoadapta.shared.domain.services.aps.Headers;
import br.com.somosdigital.bookshelf.BuildConfig;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.HEAD;
import retrofit2.http.Header;

/**
 * Created by pelisoli on 09/02/17.
 */

public interface TemplateInterface {

    // Books
    @HEAD(BuildConfig.TEMPLATE_VERSION + "/templates.zip")
    Observable<Response<Void>> getActivity(@Header(Headers.IfNoneMatch) String version);

}
