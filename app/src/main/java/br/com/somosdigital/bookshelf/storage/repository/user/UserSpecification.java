package br.com.somosdigital.bookshelf.storage.repository.user;


import java.util.List;

import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;

/**
 * Created by pelisoli on 14/09/16.
 */
public interface UserSpecification extends Repository<BookshelfUser> {

    List<BookshelfUser> getUsers();

    BookshelfUser getById(String id);

}
