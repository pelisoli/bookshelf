package br.com.somosdigital.bookshelf.service.template;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by pelisoli on 09/02/17.
 */

public class TemplateService {

    private TemplateInterface activityService;

    public TemplateService(TemplateInterface activityService) {
        this.activityService = activityService;
    }

    public Observable<Response<Void>> getActivity(String eTag){
        return activityService.getActivity(eTag);
    }
}
