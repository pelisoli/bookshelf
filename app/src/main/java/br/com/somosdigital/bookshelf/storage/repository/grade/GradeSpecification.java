package br.com.somosdigital.bookshelf.storage.repository.grade;

import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Grade;

/**
 * Created by pelisoli on 16/09/16.
 */
public interface GradeSpecification extends Repository<Grade> {
}
