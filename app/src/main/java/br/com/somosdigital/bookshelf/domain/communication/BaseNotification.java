package br.com.somosdigital.bookshelf.domain.communication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import timber.log.Timber;

/**
 * Created by pelisoli on 01/06/17.
 */

public class BaseNotification {
    private static final String ACTION = "action";
    private static final String DATA = "data";

    public static JSONObject build(int protocolId, List<String> userIds){
        JSONObject message = new JSONObject();
        JSONArray studentsJSONArray = new JSONArray(userIds);

        try {
            message.put(ACTION, protocolId);
            message.put(DATA, studentsJSONArray);
        } catch (JSONException e) {
            Timber.e(e.getMessage());
        }

        return message;
    }
}
