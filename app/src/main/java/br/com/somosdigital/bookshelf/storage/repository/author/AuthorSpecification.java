package br.com.somosdigital.bookshelf.storage.repository.author;


import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Author;

/**
 * Created by pelisoli on 16/09/16.
 */
public interface AuthorSpecification extends Repository<Author> {
}
