package br.com.somosdigital.bookshelf.presentation.bookshelf.books.card;

/**
 * Created by marcus on 10/10/16.
 */

public interface ProgressAction {
    void onProgressUpdate(float progress);

    void spinProgress();
}
