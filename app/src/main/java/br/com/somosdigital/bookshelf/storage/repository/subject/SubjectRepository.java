package br.com.somosdigital.bookshelf.storage.repository.subject;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Subject;
import io.realm.Realm;

/**
 * Created by pelisoli on 16/09/16.
 */
public class SubjectRepository implements SubjectSpecification {
    Realm realm;

    public SubjectRepository(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void add(Subject item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<Subject> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(Subject item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<Subject> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(Subject item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realm1 -> item.deleteFromRealm());
        }
    }

    private boolean checkRealmInstance(Realm realm){
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }
}
