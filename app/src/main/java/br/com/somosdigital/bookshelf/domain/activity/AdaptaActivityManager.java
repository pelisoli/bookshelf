package br.com.somosdigital.bookshelf.domain.activity;

import java.io.File;

import br.com.somosdigital.bookshelf.BuildConfig;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BooksValues;

/**
 * Created by pelisoli on 06/12/16.
 */
public class AdaptaActivityManager implements AdaptaActivity {

    ActivityInfo activityInfo;

    private String apsUrl;

    private String apsVersion;

    private String templateVersion;

    private String webActivityUrl;

    public AdaptaActivityManager(
            ActivityInfo activityInfo,
            String apsUrl,
            String apsVersion,
            String templateVersion,
            String webActivityUrl) {

        this.activityInfo = activityInfo;
        this.apsUrl = apsUrl;
        this.apsVersion = apsVersion;
        this.templateVersion = templateVersion;
        this.webActivityUrl = webActivityUrl;

        if (activityInfo != null &&
                activityInfo.getActivityType() != null &&
                activityInfo.getActivityType().contentEquals("multipla")) {
            activityInfo.setActivityType("multipla-escolha");
        }
    }

    public AdaptaActivityManager(ActivityInfo activityInfo) {
        this.activityInfo = activityInfo;
    }

    @Override
    public String getLocalPath(boolean isTeacher, boolean canSendActivty) {
        String path = "";

        if (activityInfo != null) {
            String localActivityPath = "templates/" + activityInfo.getActivityType() + "/index.html";
            String localActivityQuery = "?page=" + activityInfo.getBookPage() + "&activityID="
                    + activityInfo.getActivityId() + "&type=tablet" + "&preview=" +
                    (isTeacher && canSendActivty ? "false" : "true");

            String localActivityFile = BooksValues.getInstance().getTemplatePath() + File.separator;

            path = "file://" + localActivityFile + localActivityPath + localActivityQuery;
        }

        return path;
    }

    @Override
    public String getWebPath(boolean isTeacher, boolean canSendActivty) {
        String path = "";

        if (activityInfo != null) {
            String webActivityPath = "templates/" + templateVersion + "/" + activityInfo.getActivityType() + "/index.html";
            String webActivityQuery = "";

            if (activityInfo.isAutoria() && activityInfo.getContentId() != null) {
                webActivityQuery = "?urlAPI=" + apsUrl + "activities/" + apsVersion +
                        "/content_editions/latest?content_id=&schoolWork=&" + "classRunning=false&preview=" +
                        (isTeacher && canSendActivty ? "false" : "true") +
                        "&type=tablet";
                webActivityQuery = webActivityQuery + "&contentID=" + activityInfo.getActivityId();
            } else {
                webActivityQuery = "?urlAPI=" + apsUrl + "activities/" + apsVersion +
                        "/content_editions/&schoolWork=&" + "classRunning=false&preview=" +
                        (isTeacher && canSendActivty ? "false" : "true") + "&type=tablet";

                webActivityQuery = webActivityQuery + "&activityID=" + activityInfo.getActivityId();
            }

            path = webActivityUrl + webActivityPath + webActivityQuery;
        }

        return path;
    }

    @Override
    public String getLocalTemplate(boolean isTeacher, boolean canSendActivty) {
        String localActivityPath = "";
        String webActivityQuery = "";

        if (activityInfo != null) {
            localActivityPath = "templates/" + activityInfo.getActivityType() + "/index.html";

            if (activityInfo.isAutoria() && activityInfo.getContentId() != null) {
                webActivityQuery = "?urlAPI=" + apsUrl + "activities/" + apsVersion +
                        "/content_editions/latest?content_id=&schoolWork=&" + "classRunning=false&preview="
                        + (isTeacher && canSendActivty ? "false" : "true") + "&type=tablet";
                webActivityQuery = webActivityQuery + "&contentID=" + activityInfo.getActivityId();
            } else {
                webActivityQuery = "?urlAPI=" + apsUrl + "activities/" + apsVersion +
                        "/content_editions/&schoolWork=&" + "classRunning=false&preview=" 
                        + (isTeacher && canSendActivty ? "false" : "true") + "&type=tablet";
                webActivityQuery = webActivityQuery + "&activityID=" + activityInfo.getActivityId();
            }
        }

        String localActivityFile = BooksValues.getInstance().getTemplatePath() + File.separator;

        return "file://" + localActivityFile + localActivityPath + webActivityQuery;
    }

    @Override
    public boolean templateExists() {
        File templateFile = new File(BooksValues.getInstance().getTemplatePath());
        return templateFile.exists();
    }

    @Override
    public boolean activityExists() {
        String localActivityFile = getLocalActivityPath();
        File activityFile = new File(localActivityFile);
        boolean activityExists = activityFile.exists();
        return activityExists;
    }

    public ActivityInfo getActivityInfo() {
        return activityInfo;
    }

    @Override
    public String getLocalActivityPath() {
        return BooksValues.getInstance().getBookRootPath() +
                File.separator + activityInfo.getBookId() +
                File.separator +
                "activities" +
                File.separator +
                activityInfo.getActivityId();
    }

    @Override
    public String getActivityJsonPath() {

        String localActivityJsonPath = getLocalActivityPath();
        localActivityJsonPath = localActivityJsonPath + File.separator + "activity.json";

        return localActivityJsonPath;
    }

    @Override
    public String getActivityResourcesPath() {
        if (templateExists() && activityExists()) {
            return BooksValues.getInstance().getBookRootPath() + File.separator + activityInfo.getBookId();
        } else {
            return BuildConfig.WEB_ACTIVITIES_URL;
        }
    }
}
