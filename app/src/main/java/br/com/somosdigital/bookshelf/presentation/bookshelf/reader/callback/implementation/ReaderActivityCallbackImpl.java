package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.implementation;

import android.webkit.JavascriptInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.somosdigital.bookshelf.presentation.bookshelf.activity.ReaderActivity;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderTemplate;

/**
 * Created by jgsmanaroulas on 11/1/16.
 */
public class ReaderActivityCallbackImpl implements ReaderTemplate {

    private ReaderActivity readerActivity;

    private ISendActivity sendActivity;

    public ReaderActivityCallbackImpl(ReaderActivity readerActivity, ISendActivity sendActivity) {
        this.readerActivity = readerActivity;
        this.sendActivity = sendActivity;
    }

    @JavascriptInterface
    @Override
    public String getClassInfo() {
       return readerActivity.onGetClassInfo();
    }

    /**
     *
     * Parametros de entrada:
     * activity: json contendo
     * page: Pagina da atividade;
     * activityId: ID da activity (ID de dentro do activity.json da atividade);
     * type: "tablet" | "brainhoney"
     * activityType: tipo da atividade (dissertativa, multipla escolha, etc);
     * activityTitle: titulo da atividade.
     * stat: json de statement
     * Retorno: void
     * Envia-se a atividade para o Adapta Aluno ou para o AVA dependendo do parametro activityType.
     * Envia-se o statement recebido para o LRS (se der erro no envio para o LRS, o statement
     * é salvo para envio posterior)
     * Obs.: O aplicativo do professor para iOS diferencia o envio de atividade casa ou para aula
     * pela existencia do lessonId dentro do json: caso existir, será considerada uma atividade
     * de aula (consequentemente será salvo no APS dentro de class_activity).
     * O sentDate é preenchido dentro do próprio template. O aplicativo deve ler
     * o template a ser enviado para obter tal informação para envá-la no APS (no recurso
     * class_activity ou home_activity).
     *
     * */
    @JavascriptInterface
    @Override
    public void sendActivity(String activity, String statement) {
        sendActivity.sendActivity(activity, statement);
    }

    @JavascriptInterface
    @Override
    public String getAllActivityResults(String url) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("statements", new JSONArray());
        return jsonObject.toString();
    }

    @JavascriptInterface
    @Override
    public String saveActivity(String statements) {
        readerActivity.saveActivity(statements);
        return "{}";
    }

    @JavascriptInterface
    @Override
    public String saveActivityKeepOpen(String statements) {
        readerActivity.saveActivity(statements);
        return "{}";
    }

    @JavascriptInterface
    @Override
    public void closeActivity() {

    }
}
