package br.com.somosdigital.bookshelf.domain.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import br.com.converge.core.crypt.model.RSAKeyPair;
import br.com.converge.encryption.android.RSA;
import br.com.solucaoadapta.shared.storage.preferences.PreferencesManager;
import timber.log.Timber;

public class DrmClient {
    private PreferencesManager preferencesManager;

    public DrmClient(Context context, String preferenceName) {
        this.preferencesManager = new PreferencesManager(context, preferenceName);
    }

    public RSAKeyPair getKeys() {
        RSAKeyPair keys = null;
        String KEY_NAME = "keys";
        String keyString = preferencesManager.getString(KEY_NAME);

        if (keyString != null) {
            Gson gson = new Gson();
            keys = gson.fromJson(keyString, RSAKeyPair.class);
        } else {
            RSA rsa = new RSA();
            try {
                keys = rsa.createKeys();
            } catch (Exception ex) {
                if (ex != null && ex.getStackTrace() != null) {
                    Timber.e( Log.getStackTraceString(ex));
                }
            }

            try {
                preferencesManager.setString(KEY_NAME, new Gson().toJson(keys));
            } catch (Exception e) {
                // TODO: tratamento?
                if (e != null && e.getStackTrace() != null) {
                    Timber.e( Log.getStackTraceString(e));
                }
            }
        }

        return keys;
    }
}
