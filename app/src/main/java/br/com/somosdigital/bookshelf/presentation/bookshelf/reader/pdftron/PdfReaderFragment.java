package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pdftron.common.PDFNetException;
import com.pdftron.fdf.FDFDoc;
import com.pdftron.pdf.Annot;
import com.pdftron.pdf.Bookmark;
import com.pdftron.pdf.PDFDoc;
import com.pdftron.pdf.PDFNet;
import com.pdftron.pdf.PDFViewCtrl;
import com.pdftron.pdf.Page;
import com.pdftron.pdf.TextSearchResult;
import com.pdftron.pdf.annots.Markup;
import com.pdftron.pdf.controls.AnnotationDialogFragment;
import com.pdftron.pdf.controls.AnnotationPropertyPopupWindow;
import com.pdftron.pdf.controls.SearchResultsPopupWindow;
import com.pdftron.pdf.controls.ThumbnailSlider;
import com.pdftron.pdf.controls.ThumbnailsViewFragment;
import com.pdftron.pdf.controls.UserBookmarkDialogFragment;
import com.pdftron.pdf.tools.AnnotCollabManager;
import com.pdftron.pdf.tools.FreehandCreate;
import com.pdftron.pdf.tools.TextHighlighter;
import com.pdftron.pdf.tools.Tool;
import com.pdftron.pdf.tools.ToolManager;
import com.pdftron.pdf.utils.AnalyticsHandlerAdapter;

import org.spongycastle.crypto.InvalidCipherTextException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.converge.encryption.android.RSA;
import br.com.solucaoadapta.shared.presentation.dialog.information.InformationDialog;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.bookshelf.reader.PdfReaderInteractor;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BooksValues;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.DeviceHelper;
import br.com.somosdigital.bookshelf.domain.cryptography.CryptographyManager;
import br.com.somosdigital.bookshelf.domain.utils.DrmClient;
import br.com.somosdigital.bookshelf.domain.utils.state.ApiState;
import br.com.somosdigital.bookshelf.domain.utils.state.BookshelfLibraryState;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.controls.MyPdfCtrl;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.GoToPageFragment;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.IndexDialogFragment;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.OedsDialogFragment;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.activities_list.ActivitiesDialogFragment;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.FreeHandProperties;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;
import timber.log.Timber;

import static com.pdftron.pdf.tools.AnnotCollabManager.ANNOTATION_CONSTANTS_ADD;
import static com.pdftron.pdf.tools.AnnotCollabManager.ANNOTATION_CONSTANTS_DELETE;
import static com.pdftron.pdf.tools.AnnotCollabManager.ANNOTATION_CONSTANTS_MODIFY;
import static com.pdftron.pdf.tools.AnnotCollabManager.ANNOTATION_CONSTANTS_NEW;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_ADD_END;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_ADD_START;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_DELETE_END;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_DELETE_START;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_MODIFY_END;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_MODIFY_START;

/**
 * Created by jgsmanaroulas on 20/01/17.
 */
public class PdfReaderFragment extends Fragment implements ThumbnailSlider.ThumbnailSliderListener,
        PDFViewCtrl.PageChangeListener, IndexDialogFragment.IndexDialogListener,
        ActivitiesDialogFragment.ActivitiesDialogFragmentListener, MyPdfCtrl.OnSingleTapListener,
        PdfReaderContract.View, GoToPageFragment.GoToPageListener, OedsDialogFragment.OedsDialogFragmentListener,
        ThumbnailsViewFragment.ThumbnailsViewFragmentListener,
        UserBookmarkDialogFragment.UserBookmarkDialogFragmentListener {

    private static boolean loadOedOffline;

    private SearchResultsPopupWindow mSearchPopup;

    private PdfReaderPresenter presenter;

    private ToolManager mToolManager;

    private boolean isShowingControllers = true;

    private List<BookActivityInfo> activitiesList;

    private String bookRoot;

    private PdfReaderCommunication fragmentCommunication;

    protected MyPdfCtrl pdfView;

    protected ImageView imvListIndex;

    protected ThumbnailSlider thumbnailSlider;

    protected TextView txvPageCount;

    protected SearchView searchView;

    protected ImageView imvFreeHand;

    protected ImageView imvListAnnotations;

    protected ImageView imvToggleAnnotations;

    protected ImageView imvThumbnails;

    protected ImageView imvBookmark;

    private FreeHandProperties choosenHandProperties;

    private FloatingActionButton fabActivities;

    private FloatingActionButton fabOeds;

    private ProgressBar loadingView;

    private String bookId;

    private String userId;

    private boolean hasOeds;

    private boolean hasActivities;

    private boolean isAnnotationsEnabled;

    private boolean showActivities;

    private boolean canSendActivities;

    private PDFDoc baseDoc;

    private String url;

    private LinearLayout errorView;

    private boolean isFirstLoad = true;

    private String token;

    private List<OedInfo> oedList;

    private Map<String, String> addCommands = new HashMap<>();
    private Map<String, String> modifyCommands = new HashMap<>();
    private Map<String, String> removeCommands = new HashMap<>();
    private Map<String, String> addCommandsBKP = new HashMap<>();
    private Map<String, String> modifyCommandsBKP = new HashMap<>();
    private Map<String, String> removeCommandsBKP = new HashMap<>();

    private HashMap<Integer, List<BookActivityInfo>> activityMap = new HashMap<>();

    // TODO
    // Ajuste para requisições feitas para a API do Plurall
    // Será retirado
    private String client;
    private boolean hasError = false;
    private TextView txtErrorSync;

    public void setClient(String client) {
        this.client = client;
    }


    public static PdfReaderFragment newInstance(String userId,
                                                String bookId,
                                                boolean isAnnotationsEnabled,
                                                boolean showActivities,
                                                boolean hasActivities,
                                                boolean canSendActivities,
                                                boolean loadOedOffline) {
        Bundle bundle = new Bundle();
        bundle.putString("userId", userId);
        bundle.putString("bookId", bookId);
        bundle.putBoolean("isAnnotationsEnabled", isAnnotationsEnabled);
        bundle.putBoolean("showActivities", showActivities);
        bundle.putBoolean("hasActivities", hasActivities);
        bundle.putBoolean("loadOedOffline", loadOedOffline);
        bundle.putBoolean("canSendActivities", canSendActivities);

        PdfReaderFragment fragment = new PdfReaderFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    public static PdfReaderFragment newInstance(String userId,
                                                String bookId,
                                                String url,
                                                String token) {

        Bundle bundle = new Bundle();
        bundle.putString("userId", userId);
        bundle.putString("bookId", bookId);
        bundle.putString("url", url);
        bundle.putString("token", token);
        bundle.putBoolean("isAnnotationsEnabled", false);
        bundle.putBoolean("showActivities", false);
        bundle.putBoolean("hasActivities", false);
        bundle.putBoolean("canSendActivities", false);

        PdfReaderFragment fragment = new PdfReaderFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            PDFNet.initialize(this.getActivity(), R.raw.pdfnet, "EDUMOBI Tecnologia de Ensino Móvel Ltda(plurall.net):OEM:Plurall::IA:AMS(20180712):EB6703D01FD75AD08333FD7860617F85DF166A33ED0435DA5C858B3D0641B6F5C7");
        } catch (PDFNetException e) {
            //TODO: tratamento caso o Reader lance uma exception?
            Timber.e("Erro na inicialização do PDFNet");
            e.printStackTrace();
        }

        View view = inflater.inflate(R.layout.pdftron, container, false);

        getBasicInfo(getArguments());

        configureViews(view);

        configurePdfTron();


        PdfReaderInteractor interactor = new PdfReaderInteractor(
                bookRoot,
                bookId,
                userId,
                DeviceHelper.getDevice(getContext()),
                CryptographyManager.getPublicKey(getContext(), BookshelfLibraryState.getPreferenceName()),
                fragmentCommunication.getLog(),
                fragmentCommunication.getApplicationLocalPreferences(),
                hasActivities,
                showActivities,
                ApiState.getDistribuidorService());

        presenter = new PdfReaderPresenter(fragmentCommunication.getLog(), interactor);

        presenter.attachView(this);
        presenter.loadPDFDocument(url);

        return view;
    }


    private void configureViews(View view) {
        final boolean[] drawAnnotations = {true};

        errorView = view.findViewById(R.id.error_view);
        txtErrorSync = errorView.findViewById(R.id.txt_error_sync);

        if (isAnnotationsEnabled) {
            errorView.setPivotY(0);
            errorView.clearAnimation();
            errorView.setScaleY(0);
            errorView.setVisibility(View.VISIBLE);

            Button btnReSend = errorView.findViewById(R.id.btn_try);
            btnReSend.setOnClickListener(button -> {
                setAnnotationErrorVisibility(false);
                if (hasBackupAnnotations()) {
                    presenter.sendBackupAnnotations(addCommandsBKP, modifyCommandsBKP, removeCommandsBKP);
                }
                presenter.sendAnnotaionUpdate(addCommands, modifyCommands, removeCommands);
            });

            Button btnDiscard = errorView.findViewById(R.id.btn_discard);
            btnDiscard.setOnClickListener(button -> discardAnnotations());
        } else {
            errorView.setVisibility(View.GONE);
        }

        pdfView = view.findViewById(R.id.pdfviewctrl);
        imvListIndex = view.findViewById(R.id.reader_controller_imv_index);
        thumbnailSlider = view.findViewById(R.id.reader_controller_thumbnailslider);
        txvPageCount = view.findViewById(R.id.reader_controller_page_counter);
        searchView = view.findViewById(R.id.reader_searchview);
        fabActivities = view.findViewById(R.id.pdftron_fab_activities);
        fabOeds = view.findViewById(R.id.pdftron_fab_oeds);
        imvFreeHand = view.findViewById(R.id.reader_imv_freehand);
        imvListAnnotations = view.findViewById(R.id.reader_imv_list_annotations);
        imvToggleAnnotations = view.findViewById(R.id.reader_imv_toggle_annotations);
        imvThumbnails = view.findViewById(R.id.reader_imv_thumbnails);
        imvBookmark = view.findViewById(R.id.reader_imv_bookmark);
        loadingView = view.findViewById(R.id.progress_bar);

        imvBookmark.setVisibility(View.GONE);
//        imvBookmark.setOnClickListener(v -> showBookmarksFragment());
        imvThumbnails.setOnClickListener(v -> showThumbnailsFragment());

        //Toggle para desenhar (ou não) as anotações;
        imvToggleAnnotations.setOnClickListener(v -> {
            try {
                drawAnnotations[0] = !drawAnnotations[0];
                pdfView.setDrawAnnotations(drawAnnotations[0]);
            } catch (PDFNetException e) {

                Timber.e("Erro ao habilitar/desabilitar o desenho de anotacoes");
                e.printStackTrace();
            }
            if (drawAnnotations[0]) {
                imvToggleAnnotations.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_eye));
                Toast.makeText(getContext(), getString(R.string.reader_annotation_show), Toast.LENGTH_SHORT).show();
            } else {
                imvToggleAnnotations.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_eye_off));
                Toast.makeText(getContext(), getString(R.string.reader_annotation_hide), Toast.LENGTH_SHORT).show();
            }
        });


        imvFreeHand.setColorFilter(getFreehandTint(), PorterDuff.Mode.SRC_ATOP);
        imvFreeHand.setOnClickListener(l -> {
            //Se, ao clicar no icone de anotação free hand, a ferramenta sendo usada for a Freehand, devemos commitar
            //a anotação e voltar para a ferramenta de "navegação" (pan).
            if (mToolManager.getTool() instanceof FreehandCreate) {
                ((FreehandCreate) mToolManager.getTool()).commitAnnotation();
                mToolManager.setTool(mToolManager.createTool(ToolManager.e_pan, null));
                imvFreeHand.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.annotation_free_hand));
            } else {
                //Caso contrário, habilitamos a ferramente de Freehand (e_ink_create)
                mToolManager.setTool(mToolManager.createTool(ToolManager.e_ink_create, null));
                //Desabilitamos uma flag que força o uso da mesma ferramenta;
                ((Tool) mToolManager.getTool()).setForceSameNextToolMode(false);
                //Habilitamos uma flag que permite o usuário fazer vários desenhos de uma vez só;
                ((FreehandCreate) mToolManager.getTool()).setMultiStrokeMode(true);
                imvFreeHand.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.dismiss));
                //Se o usuário já tinha editado as propriedades da ferramenta de freehand, usamos.
                if (choosenHandProperties != null) {
                    int color = choosenHandProperties.getColor();
                    int fill = choosenHandProperties.getFill();
                    float opacity = choosenHandProperties.getOpacity();
                    float thickness = choosenHandProperties.getThickness();
                    String icon = choosenHandProperties.getIcon();
                    String pdftronFontName = choosenHandProperties.getPdfTronFontName();
                    ((FreehandCreate) mToolManager.getTool()).setupAnnotProperty(color, opacity, thickness, fill, icon, pdftronFontName);
                }
                Toast.makeText(getContext(), getString(R.string.reader_freehand_hint_pref), Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext(), getString(R.string.reader_freehand_hint_close), Toast.LENGTH_SHORT).show();
            }

        });


        //Long click no ícone de freehand exibe a janela de 'configuração' da ferramenta;
        //Ao ser fechada, pegamos as configurações selecionadas e armazenamos em um objeto para que, ao usarmos novamente,
        //possamos reconstruir a ferramenta com as mesma configurações.
        imvFreeHand.setOnLongClickListener(l -> {
            final AnnotationPropertyPopupWindow popupWindow = new AnnotationPropertyPopupWindow(getActivity(), ToolManager.e_ink_create);
            popupWindow.setOnDismissListener(() -> {
                if (mToolManager == null) {
                    return;
                }
                popupWindow.prepareDismiss();
                int color = popupWindow.getColor();
                int fill = popupWindow.getFillColor();
                float thickness = popupWindow.getThickness();
                float opacity = popupWindow.getOpacity();
                String icon = popupWindow.getIcon();
                String pdftronFontName = popupWindow.getPDFTronFontName();
                choosenHandProperties = new FreeHandProperties(color, fill, thickness, opacity, icon, pdftronFontName);
                imvFreeHand.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                if (mToolManager.getTool() instanceof FreehandCreate) {
                    ((FreehandCreate) mToolManager.getTool()).setupAnnotProperty(color, opacity, thickness, fill, icon, pdftronFontName);
                }
            });
            popupWindow.show(imvFreeHand);
            return true;
        });

        imvListIndex.setOnClickListener(view1 -> {
            IndexDialogFragment frag = IndexDialogFragment.newInstance(pdfView, this);
            frag.show(getActivity().getSupportFragmentManager(), IndexDialogFragment.TAG);
        });

        imvListAnnotations.setOnClickListener(view2 -> {
            AnnotationDialogFragment frag = AnnotationDialogFragment.newInstance(pdfView, userId);
            frag.show(getActivity().getSupportFragmentManager(), AnnotationDialogFragment.TAG);
        });


        txvPageCount.setOnClickListener(view12 -> {
            GoToPageFragment frag = GoToPageFragment.newInstance(this);
            frag.show(getActivity().getSupportFragmentManager(), GoToPageFragment.TAG);
        });

        fabActivities.setOnClickListener(view13 -> {

            ActivitiesDialogFragment frag = ActivitiesDialogFragment.newInstance(this, fragmentCommunication);

            Bundle b = new Bundle();
            for (BookActivityInfo activityInfo : activitiesList) {
                activityInfo.setSelected(false);
            }
            b.putParcelableArrayList("activities", (ArrayList<BookActivityInfo>) activitiesList);
            b.putBoolean("canSendActivities", canSendActivities);

            frag.setArguments(b);
            frag.show(getActivity().getSupportFragmentManager(), ActivitiesDialogFragment.TAG);
        });

        fabOeds.setOnClickListener(view14 -> {
            OedsDialogFragment frag = OedsDialogFragment.newInstance(this);

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(OedsDialogFragment.BUNDLE_OEDS, (ArrayList<OedInfo>) oedList);

            frag.setArguments(bundle);
            frag.show(getActivity().getSupportFragmentManager(), OedsDialogFragment.TAG);
        });

        searchView.setFocusable(false);
        searchView.setFocusableInTouchMode(false);
        searchView.setIconified(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                findText(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchClickListener(view15 -> {
//            imvBookmark.setVisibility(View.INVISIBLE);
            imvThumbnails.setVisibility(View.INVISIBLE);

            if (isAnnotationsEnabled) {
                imvToggleAnnotations.setVisibility(View.GONE);
                imvFreeHand.setVisibility(View.GONE);
                imvListAnnotations.setVisibility(View.GONE);
            }
        });

        searchView.setOnCloseListener(() -> false);

        ImageView searchCloseButton = this.searchView.findViewById(R.id.search_close_btn);
        searchCloseButton.setOnClickListener(v -> {
            searchView.onActionViewCollapsed();
            exitSearchMode();
            hideKeyboard();
        });

        mSearchPopup = new SearchResultsPopupWindow(getActivity(), null);
        mSearchPopup.showSearchView(false);
        mSearchPopup.setListener(new SearchResultsPopupWindow.SearchResultsPopupListener() {
            @Override
            public void onSearchPopupWindowShow() {
                hideKeyboard();
            }

            @Override
            public void onSearchPopupWindowDismiss() {
                //
            }

            @Override
            public void onSearchResultClicked(TextSearchResult textSearchResult) {
                mSearchPopup.dismiss();
                mSearchPopup.cancelSearch();
                pdfView.requestFocus();
                pdfView.selectAndJumpWithHighlights(textSearchResult.getHighlights());
                if (mToolManager != null && mToolManager.getTool() instanceof TextHighlighter) {
                    TextHighlighter highlighter = (TextHighlighter) mToolManager.getTool();
                    highlighter.update();
                }
            }

            @Override
            public void onFullTextSearchStart() {
                //
            }

            @Override
            public void onSearchResultFound(TextSearchResult textSearchResult) {
                //
            }
        });

        if (isAnnotationsEnabled) {
            createToolManager();

            mToolManager.setAuthorId(userId);

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(pdfView.getContext());
            //pref_author_name é a key usada no projeto Tools do PDFTron para edição/criação de anotações
            pref.edit().putString("pref_author_name", userId).commit();

            mToolManager.setToolChangedListener((newTool, oldTool) -> {
                //Assim que saimos da Tool "FreehandCreate" (usada para desenho livre na tela) devemos commitar a anotação
                //para que ela seja salva conforme esperado;
                if (oldTool instanceof FreehandCreate) {
                    try {
                        ((FreehandCreate) oldTool).commitAnnotation();
                    } catch (Exception e) {
                        AnalyticsHandlerAdapter.getInstance().sendException(e);
                    }
                }
            });
            //Não habilitamos a edição de desenhos logo após comita-los
            ToolManager.setEditInkAnnots(false);
            //Não usamos a toolbar do projeto tools
            ToolManager.setOpenToolbarOnPanInkSelected(false);

            //Flag que indica se o 'menu de contexto' (para edição) ao criar uma anotação deve ser exibido logo após a criação da mesma;
            mToolManager.setShowQuickMenu(false);

            AnnotCollabManager collabManager = new AnnotCollabManager(mToolManager, (action, uuid, xfdfCommand, annotParams) -> {
                if (xfdfCommand != null && !xfdfCommand.isEmpty()) {
                    switch (action) {
                        case ANNOTATION_CONSTANTS_NEW:
                        case ANNOTATION_CONSTANTS_ADD:
                            presenter.registerAnnotation(PdfReaderContract.addName, addCommands, uuid, xfdfCommand, XFDF_TAG_ADD_START, XFDF_TAG_ADD_END);
                            break;
                        case ANNOTATION_CONSTANTS_MODIFY:
                            presenter.registerAnnotation(PdfReaderContract.modifyName, modifyCommands, uuid, xfdfCommand, XFDF_TAG_MODIFY_START, XFDF_TAG_MODIFY_END);
                            break;
                        case ANNOTATION_CONSTANTS_DELETE:
                            presenter.registerAnnotation(PdfReaderContract.removeName, removeCommands, uuid, xfdfCommand, XFDF_TAG_DELETE_START, XFDF_TAG_DELETE_END);
                            break;
                    }
                } else {
                    Log.e("ERRO ANNOT", "Erro anotação action: \"" + action + "\", command: \n" + xfdfCommand);
                }

                Log.d("ANNOTATION EVENT", "Annotation action: " + action);
                Log.d("ANNOTATION EVENT", "Annotation uuid: " + uuid);
//                Log.d("ANNOTATION EVENT", "Annotation xfdfCommand: " + xfdfCommand);
            });

            pdfView.setToolManager(mToolManager);
            showAnnotations();
        } else {
            hideAnnotations();
        }

        pdfView.setAnnotationsEnabled(isAnnotationsEnabled);

        try {
            pdfView.setDrawAnnotations(isAnnotationsEnabled);
        } catch (PDFNetException e) {
            Timber.e("Erro ao habilitar/desabilitar as anotacoes");
            e.printStackTrace();
        }

    }

    private void discardAnnotations() {
        setAnnotationErrorVisibility(false);
        clearBKPCommands();
        clearCommandsLists();
        try {
            FileInputStream is = new FileInputStream(presenter.getBookFilePath(bookId));
            baseDoc = new PDFDoc(is);
            DrmClient drmClient = new DrmClient(this.getActivity(), BookshelfLibraryState.getPreferenceName());
            String password = presenter.getDocumentPassword(drmClient);

            if (password != null) {
                if (!baseDoc.initStdSecurityHandler(password)) {
                    onPasswordError();
                }
                importAnnot();
            } else {
                importAnnot();
            }
        } catch (PDFNetException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (pdfView != null) {
            pdfView.onConfigurationChanged(newConfig);
        }
        redefinePDFViewOrientation(newConfig);
        int[] visiblePages = pdfView.getVisiblePages();
        updatePageIndicator(visiblePages);
        if (presenter != null) {
            presenter.loadPageActivities(visiblePages, activityMap);
        }
    }

    @Override
    public void onThumbSliderStartTrackingTouch() {
        //nem tme o que fazer aqui acho
    }

    @Override
    public void onThumbSliderStopTrackingTouch(int page) {
        pdfView.setCurrentPage(page);
    }


    @Override
    public void onPageChange(int oldPage, int currentPage, int state) {
        int[] visiblePages = pdfView.getVisiblePages();
        if (state == PDFViewCtrl.PAGE_CHANGE_END) {
            updatePageIndicator(visiblePages);
            presenter.loadPageActivities(visiblePages, activityMap);

            if (isAnnotationsEnabled && !isFirstLoad && hasAnnotationChanges()) {
                presenter.sendAnnotaionUpdate(addCommands, modifyCommands, removeCommands);
            }
        }
    }

    @Override
    public void onIndexClicked(Bookmark bookmark) {
        try {
            int bookmarkPage = bookmark.getAction().getDest().getPage().getIndex();
            pdfView.setCurrentPage(bookmarkPage);
        } catch (PDFNetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivitySelected(BookActivityInfo bookActivityInfo) {
        fragmentCommunication.startBookActivity(bookActivityInfo, bookId, false);
    }

    @Override
    public void onSingleTapConfirmed(MyPdfCtrl.RegionSingleTap region) {
        if (region == MyPdfCtrl.RegionSingleTap.Left) {
            pdfView.gotoPreviousPage(true);
        } else if (region == MyPdfCtrl.RegionSingleTap.Right) {
            pdfView.gotoNextPage(true);
        } else if (region == MyPdfCtrl.RegionSingleTap.Middle) {
            toggleControllers();
        }
    }

    @Override
    public void showServerMaintenanceError() {
        //TODO:
    }

    private void configurePdfTron() {
        thumbnailSlider.setPDFViewCtrl(pdfView);
        thumbnailSlider.setThumbSliderListener(this);

        //Setando a seleção de texto "padrão"
        pdfView.setTextSelectionMode(PDFViewCtrl.TEXT_SELECTION_MODE_STRUCTURAL);

        //Setando modo de visualização para que a página inteira seja exibida sem necessidade de zoom;
        pdfView.setPageViewMode(PDFViewCtrl.PAGE_VIEW_FIT_PAGE);

        pdfView.setPageChangeListener(this);
        pdfView.setSingleTapListener(this);
        pdfView.setRenderingListener(new PDFViewCtrl.RenderingListener() {
            @Override
            public void onRenderingStarted() {
            }

            @Override
            public void onRenderingFinished() {

                if (loadingView != null && loadingView.getVisibility() == View.VISIBLE) {
                    // Se o loadingView estiver visível é a primeira renderização
                    loadingView.setVisibility(View.GONE);

                    int[] visiblePages = pdfView.getVisiblePages();
                    // Ter certeza que o contador de páginas será atualizado
                    updatePageIndicator(visiblePages);
                    if (presenter != null) {
                        // Verifica se a página atual tem atividades
                        presenter.loadPageActivities(visiblePages, activityMap);
                    }
                }
            }
        });

        redefinePDFViewOrientation(getResources().getConfiguration());
        try {
            pdfView.setPageBox(Page.e_user_crop);
            pdfView.setPageRefViewMode(PDFViewCtrl.PAGE_VIEW_FIT_PAGE);

            //Habilitamos o AA (melhor qualidade na renderização)
            pdfView.setAntiAliasing(true);

            //Habilitamos o click em links;
            pdfView.setUrlExtraction(true);
            pdfView.setImageSmoothing(true); //Parece que não perdemos qualidade.

            //Calculamos o tamanho máximo dos arquivos de thumbnail gerados; (valores 100% copiados do projeto sample)
            pdfView.setZoomLimits(PDFViewCtrl.ZOOM_LIMIT_RELATIVE, 1.0, 4.0);
            pdfView.setupThumbnails(false,                         // Do not use embedded thumbs
                    true,                                           // Generate thumbs at runtime
                    false,                                           // Use persistent thumb cache
                    0,                                              // 0 -> max thumb size is defined by the SDK
                    50 * 1024 * 1024,                                   // Max absolute size for the thumb cache
                    0.1);                                           // Max percentage of free disk we allow to the cache

            pdfView.setHighlightFields(true);
            pdfView.setRenderedContentCacheSize((long) (Runtime.getRuntime().maxMemory() / (1024 * 1024) * 0.25));
        } catch (PDFNetException e) {
            //error
            e.printStackTrace();
        }
    }

    @Override
    public void onLocalDocumentReady(int lastPage) {

        try {
            FileInputStream is = new FileInputStream(presenter.getBookFilePath(bookId));
            baseDoc = new PDFDoc(is);
            PDFDoc doc = baseDoc;

            DrmClient drmClient = new DrmClient(this.getActivity(), BookshelfLibraryState.getPreferenceName());
            String password = presenter.getDocumentPassword(drmClient);

            if (password != null) {
                if (doc.initStdSecurityHandler(password)) {
                    pdfView.setDoc(doc);
                    pdfView.setCurrentPage(lastPage);
                    presenter.preloadActivities();
                    presenter.preloadOeds();
                } else {
                    onPasswordError();
                }
            } else {
                pdfView.setDoc(doc);
                pdfView.setCurrentPage(lastPage);
                presenter.preloadActivities();
                presenter.preloadOeds();
            }


            if (isAnnotationsEnabled) {
                presenter.downloadXFDF();
                addCommandsBKP = presenter.getChangeList(PdfReaderContract.addName);
                modifyCommandsBKP = presenter.getChangeList(PdfReaderContract.modifyName);
                removeCommandsBKP = presenter.getChangeList(PdfReaderContract.removeName);
            }

            changeSearchVisibility(View.VISIBLE);
            changeIndexVisibility(View.VISIBLE);
        } catch (PDFNetException | IOException e) {
            showLoadingDocumentError();
            Timber.e("Erro ao carregar o PDF na pdfView: " + e.getMessage());
            e.printStackTrace();
        }

        toggleControllers();
    }

    @Override
    public void openFromUrl(String bookUrl, String password, int lastPage) {

        PDFViewCtrl.HTTPRequestOptions options = new PDFViewCtrl.HTTPRequestOptions();
        try {

            options.addHeader("Authorization", token);
            if (client != null && !client.equals("")) {
                options.addHeader("Client", client);
            }

            String cacheFilePath = "";

            // docs
            //https://www.pdftron.com/pdfnet/mobile/docs/Android/pdftron/PDF/PDFViewCtrl.DocumentDownloadListener.html
            // Erro quando a URL está expirada
            /*type: 5
            page num: 0
            page downloaded: 0
            page count: 0
            message: Server returned an HTTP error code (403):*/
            pdfView.setDocumentDownloadListener((type, page_num, page_downloaded, page_count, message) -> {
                /*fragmentCommunication.getLog().logDebug("type: " + type);
                fragmentCommunication.getLog().logDebug("page num: " + page_num);
                fragmentCommunication.getLog().logDebug("page downloaded: " + page_downloaded);
                fragmentCommunication.getLog().logDebug("page count: " + page_count);
                fragmentCommunication.getLog().logDebug("message: " + message);*/

                if (type == PDFViewCtrl.DOWNLOAD_FAILED) {
                    showLoadingDocumentError();
                }
            });

            DrmClient drmClient = new DrmClient(this.getActivity(), BookshelfLibraryState.getPreferenceName());
            //String password = presenter.getDocumentPassword(drmClient);
            if (password != null && !password.equals("")) {
                try {
                    password = (new RSA()).decrypt(password, drmClient.getKeys().getPrivateKey());
                } catch (IOException e) {
                    e.printStackTrace();
                    password = "";
                } catch (InvalidCipherTextException e) {
                    e.printStackTrace();
                    password = "";
                }
            }

            pdfView.setCurrentPage(lastPage);
            pdfView.openUrlAsync(bookUrl, cacheFilePath, password, options);
            changeSearchVisibility(View.VISIBLE);
            changeIndexVisibility(View.VISIBLE);
        } catch (PDFNetException e) {
            e.printStackTrace();
        }

        toggleControllers();
    }

    @Override
    public void onDocumentLoading() {
        changeIndexVisibility(View.GONE);
        changeSearchVisibility(View.GONE);
    }

    @Override
    public void onOedsLoaded(List<OedInfo> oeds) {
        this.oedList = oeds;
        hasOeds = true;
        fabOeds.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPasswordError() {
        //TODO:

    }

    @Override
    public void onLoadActivitiesError() {
        DialogFragment dialog = InformationDialog.newInstance(getString(R.string.reader_load_activities_error));
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onLoadOedsError() {
        DialogFragment dialog = InformationDialog.newInstance(getString(R.string.reader_load_oed_error));
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onNoOedsLoaded() {
        hasOeds = false;
        fabOeds.setVisibility(View.GONE);
    }

    private void changeIndexVisibility(int visibility) {
        if (imvListIndex != null) {
            imvListIndex.setVisibility(visibility);
        }
    }

    private void changeSearchVisibility(int visibility) {
        if (searchView != null) {
            searchView.setVisibility(visibility);
        }
    }

    @Override
    public void onActivitiesLoaded(List<BookActivityInfo> activitiesList) {
        this.activitiesList = activitiesList;
        if (activitiesList == null || activitiesList.isEmpty()) {
            fabActivities.setVisibility(View.INVISIBLE);
        } else {
            fabActivities.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoadingDocumentError() {
        if (loadingView.getVisibility() == View.VISIBLE) {
            loadingView.setVisibility(View.GONE);
        }
        DialogFragment dialog = InformationDialog.newInstance(getString(R.string.reader_load_book_error));
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    private void toggleControllers() {
        if (isShowingControllers) {
            setAnnotationErrorVisibility(false);
            thumbnailSlider.setVisibility(View.GONE);
            txvPageCount.setVisibility(View.GONE);
            imvListIndex.setVisibility(View.GONE);
//            imvBookmark.setVisibility(View.GONE);
            imvFreeHand.setVisibility(View.GONE);
            searchView.setVisibility(View.GONE);
            imvThumbnails.setVisibility(View.GONE);
            fabOeds.setVisibility(hasOeds ? View.VISIBLE : View.GONE);
        } else {
            setAnnotationErrorVisibility(hasError);
            imvListIndex.setVisibility(View.VISIBLE);
            thumbnailSlider.setVisibility(View.VISIBLE);
            txvPageCount.setVisibility(View.VISIBLE);
            imvListIndex.setVisibility(View.VISIBLE);
//            imvBookmark.setVisibility(View.VISIBLE);
            imvThumbnails.setVisibility(View.VISIBLE);
            searchView.setVisibility(View.VISIBLE);
            fabOeds.setVisibility(hasOeds ? View.VISIBLE : View.GONE);

            if (isAnnotationsEnabled) {
                showAnnotations();
            } else {
                hideAnnotations();
            }
        }

        isShowingControllers = !isShowingControllers;
    }

    private void updatePageIndicator(int[] visiblePages) {
        if (visiblePages.length > 0) {
            int mPageCount = pdfView.getPageCount();
            if (txvPageCount != null) {
                String pageRange;
                if (isLandscape()) {
                    if (visiblePages.length >= 2) {
                        pageRange = String.format(getResources().getString(R.string.page_range_landscape), visiblePages[0], visiblePages[1], mPageCount);
                    } else {
                        pageRange = String.format(getResources().getString(R.string.page_range_landscape), visiblePages[0] - 1, visiblePages[0], mPageCount);
                    }
                } else {
                    pageRange = String.format(getResources().getString(R.string.page_range), visiblePages[0], mPageCount);
                }
                txvPageCount.setText(pageRange);
            }
            if (thumbnailSlider != null) {
                thumbnailSlider.setProgress(visiblePages[0]);
            }
        }
    }

    /**
     * Redefine a orientação da view do PDF de acordo com a orientação do tablet
     *
     * @param configuration Objeto de configuração do Android
     */
    private void redefinePDFViewOrientation(Configuration configuration) {
        if (pdfView != null) {
            if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                pdfView.setPagePresentationMode(PDFViewCtrl.PAGE_PRESENTATION_FACING);
            } else if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                pdfView.setPagePresentationMode(PDFViewCtrl.PAGE_PRESENTATION_SINGLE);
            }
        }
    }

    /**
     * @return Retorna se o tablet está em modo landscape
     */
    private boolean isLandscape() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (pdfView != null) {

            exitSearchMode();

            if (presenter != null) {
                presenter.saveLastReadPage(pdfView.getCurrentPage());
            }

            if (isAnnotationsEnabled) {
                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setIndeterminate(true);
                progressDialog.setTitle(getString(R.string.wait));
                progressDialog.setMessage(getString(R.string.reader_saving_annot));
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                try {
                    if (pdfView.getDoc() != null && pdfView.getDoc().isModified()) {

                        progressDialog.show();

                        savePdfFile();

                        pdfView.pause();

                    }
                } catch (PDFNetException e) {
                    Timber.e("Erro ao salvar as anotacoes: " + e.getMessage());
                    //Algo deveria ser alertado aqui?
                    e.printStackTrace();
                } finally {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (pdfView != null) {
            pdfView.resume();
        }

        if (presenter != null) {
            presenter.attachView(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (isAnnotationsEnabled) {
            Log.d("", "removendo arquivo de anotação");
            String filePath = bookRoot + File.separator + "annotations" + File.separator + userId + ".xfdf";
            File file = new File(filePath);
            if (file.exists()) file.delete();
        }

        if (presenter != null) {
            presenter.detachView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (pdfView != null) {
            pdfView.destroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        if (pdfView != null) {
            pdfView.purgeMemory();
        }
    }

    private void findText(String mSearchQuery) {
        if (mSearchQuery != null && mSearchQuery.trim().length() > 0) {
            pdfView.findText(mSearchQuery, false, false, true, false, -1);
            highlightSearchResults(mSearchQuery);
            if (mSearchPopup.getDoc() == null || mSearchPopup.getDoc() != pdfView.getDoc()) {
                // Update search popup's pdfviewctrl
                mSearchPopup.setPDFViewCtrl(pdfView);
            }

            mSearchPopup.setMatchCase(false);
            mSearchPopup.setWholeWord(false);
            mSearchPopup.findText(mSearchQuery);
            mSearchPopup.show(false);
        }
    }

    private void highlightSearchResults(String mSearchQuery) {
        String prevPattern = null;
        if (mToolManager != null && mToolManager.getTool() instanceof TextHighlighter) { // Get previous pattern, if highlighter is already running
            prevPattern = ((TextHighlighter) mToolManager.getTool()).getSearchPattern();
        }
        // Restart text-highlighter only if query changed from last time, if applicable
        // NOTE: if the highlighter is not running, it will always be started here
        if (prevPattern == null || !mSearchQuery.equals(prevPattern)) {
            if (mSearchQuery.trim().length() > 0) {
                if (mToolManager == null) {
                    createToolManager();
                }
                if (pdfView.getToolManager() == null) {
                    pdfView.setToolManager(mToolManager);
                }
                TextHighlighter highlighter = (TextHighlighter) mToolManager.createTool(ToolManager.e_text_highlighter, null);
                mToolManager.setTool(highlighter);
                highlighter.start(mSearchQuery, false, false, false);
            }
        }
    }

    private void exitSearchMode() {
        if (mToolManager != null && mToolManager.getTool() instanceof TextHighlighter) {
            TextHighlighter highlighter = (TextHighlighter) mToolManager.getTool();
            pdfView.clearSelection();
            highlighter.clear();
            if (!isAnnotationsEnabled) {
                mToolManager = null;
                pdfView.setToolManager(mToolManager);
            }
            pdfView.invalidate();
        }

        EditText editText = searchView.findViewById(R.id.search_src_text);
        // Clear text from EditText view and clear query
        editText.setText("");
        searchView.setQuery("", false);
        // Cancel search and hide progress bar
        if (pdfView != null) {
            pdfView.cancelFindText();
        }
        if (mSearchPopup != null && mSearchPopup.isActive()) {
            mSearchPopup.cancelGetResult();
        }

        if (mToolManager != null) {
            mToolManager.setTool(mToolManager.createTool(ToolManager.e_pan, null));
        }

        mSearchPopup.cancelSearch();
        mSearchPopup.dismiss();
        pdfView.requestFocus(); // Hide soft keyboard

//        imvBookmark.setVisibility(View.VISIBLE);
        imvThumbnails.setVisibility(View.VISIBLE);
        if (isAnnotationsEnabled) {
            showAnnotations();
        }
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            if (getActivity().getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public void getBasicInfo(Bundle bundle) {
        if (bundle != null) {
            String bookStoragePath = BooksValues.getInstance().getBookRootPath();
            url = bundle.getString("url");
            token = bundle.getString("token");

            bookId = bundle.getString("bookId");
            bookRoot = bookStoragePath + File.separator + bookId;
            isAnnotationsEnabled = bundle.getBoolean("isAnnotationsEnabled");
            userId = bundle.getString("userId");
            hasActivities = bundle.getBoolean("hasActivities");
            showActivities = bundle.getBoolean("showActivities");
            loadOedOffline = bundle.getBoolean("loadOedOffline");
            canSendActivities = bundle.getBoolean("canSendActivities");
        }
    }

    @Override
    public void onPageSelected(int page) {
        if (page > 0 && page <= pdfView.getPageCount()) {
            pdfView.setCurrentPage(page);
        }
    }

    @Override
    public void onOedSelected(OedInfo oedInfo) {
        oedInfo.setAllowOedDownload(loadOedOffline);
        String objectPath = bookRoot + "/oeds/" + oedInfo.getId();
        fragmentCommunication.startOed(objectPath, bookId, oedInfo);
    }

    /**
     * Salvamos todas alterações feitas no PDF
     */
    private void savePdfFile() {
        if (isAnnotationsEnabled) extractAnnot();
    }

    /**
     * Escondemos/exibimos as anotações de acordo com o usuário atual.
     * Quando uma anotação é criada, o autor dela (setado via toolmanager::authorId) pode ser recuperado via
     * markup::getTitle.
     * <p>
     * Sendo assim, comparamos o userId atual com o "title" da anotação. Se forem diferentes, escondemos a anotação.
     * <p>
     * Esse método não está sendo utilizado atualemnte devido à separação de usuários feita pelo aps
     */
    private void refreshAnnotationsVisibility() {
        if (isAnnotationsEnabled) {
            try {
                for (int i = 0; i < pdfView.getDoc().getPageCount(); i++) {
                    Page page = pdfView.getDoc().getPage(i);
                    if (page != null) {
                        if (page.isValid()) {
                            int annotationCount = page.getNumAnnots();
                            for (int j = 0; j < annotationCount; j++) {
                                Annot annotation = page.getAnnot(j);
                                if (annotation != null && annotation.isValid()) {
                                    Markup markup = new Markup(annotation);
                                    String author = markup.getTitle();
                                    boolean hidden = !author.contentEquals(userId);
                                    annotation.setFlag(Annot.e_hidden, hidden);
                                    if (hidden) {
                                        pdfView.hideAnnotation(annotation);
                                    }
                                    pdfView.update(annotation, page.getIndex());
                                }
                            }
                        }
                    }
                }
            } catch (PDFNetException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Extraimos as anotações do livro para um arquivo de 'apoio', a ser usado futuramente para recuperar as anotações do livro;
     */
    private void extractAnnot() {
        PDFDoc doc = pdfView.getDoc();
        try {
            FDFDoc docAnnots = doc.fdfExtract(PDFDoc.e_annots_only);
            String filePath = bookRoot + File.separator + "annots" + File.separator + userId + ".xfdf";
            docAnnots.saveAsXFDF(filePath);
        } catch (PDFNetException e) {
            Timber.e("Erro ao extrair as anotacoes do PDF: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Método para importamos as anotações previamente salvas, a ser usado quando o livro for, por exemplo, atualizado
     */
    @Override
    public void importAnnot() {
        try {
            int page = pdfView.getCurrentPage();
            pdfView.setDoc(baseDoc); //usa referencia do doc limpo de annotations antes do merge, evita duplicação.
            pdfView.setCurrentPage(page);
            pdfView.update(true);

            String filePath = bookRoot + File.separator + "annotations" + File.separator + userId + ".xfdf";
            FDFDoc fdfDoc = FDFDoc.createFromXFDF(filePath);
            PDFDoc doc = pdfView.getDoc();
            hasError = false;
            txtErrorSync.setText(R.string.error_annot_sync);
            if (isFirstLoad && hasBackupAnnotations()) {
                hasError = true;
                Log.d("########", "aplicando alterações de backup");
                txtErrorSync.setText(R.string.error_annot_not_sync);
                String changes = presenter.getChangesFromBKP(addCommandsBKP, modifyCommandsBKP, removeCommandsBKP);
                fdfDoc.mergeAnnots(changes);
                setAnnotationErrorVisibility(hasError);
            }
            isFirstLoad = false;
            doc.fdfMerge(fdfDoc);
        } catch (Exception e) {
            Timber.e("Erro ao importar as anotacoes do PDF: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void unsetFirstLoad() {
        isFirstLoad = false;
    }

    @Override
    public void loadBackupAnnots() {
        isFirstLoad = false;
        if (isFirstLoad && hasBackupAnnotations()) {
            try {
                hasError = true;
                FDFDoc fdfDoc = new FDFDoc();
                PDFDoc doc = pdfView.getDoc();
                String changes = presenter.getChangesFromBKP(addCommandsBKP, modifyCommandsBKP, removeCommandsBKP);
                fdfDoc.mergeAnnots(changes);
                setAnnotationErrorVisibility(hasError);
                txtErrorSync.setText(R.string.error_annot_not_sync);
                doc.fdfMerge(fdfDoc);
            } catch (PDFNetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setHasError(boolean hasError) {
        this.hasError = hasError;
        txtErrorSync.setText(R.string.error_annot_sync);
        setAnnotationErrorVisibility(hasError);
    }

    @Override
    public void setAnnotationErrorVisibility(boolean isVisible) {
        errorView.clearAnimation();
        errorView.animate().scaleY(isVisible ? 1 : 0).setDuration(200);
    }

    @Override
    public void showAnnotationDownloadError() {
        DialogFragment dialog = InformationDialog.newInstance(getString(R.string.annotation_download_error));
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void startSendingAnnotUpdate() {
        setAnnotationErrorVisibility(false);
    }

    @Override
    public void errorAnnotUpdate() {
        DialogFragment dialog = InformationDialog.newInstance(getString(R.string.annotation_update_error));
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    /**
     * @return A última cor usada na ferramenta freehand.
     */
    private int getFreehandTint() {
        SharedPreferences settings = getContext().getSharedPreferences(Tool.PREFS_FILE_NAME, 0);
        int color = settings.getInt(Tool.PREF_ANNOTATION_PROPERTY_FREEHAND + Tool.PREF_ANNOTATION_PROPERTY_CUSTOM + Tool.PREF_ANNOTATION_PROPERTY_COLOR, R.color.red);
        return color;
    }

    @Override
    public void onThumbnailsViewDialogDismiss(int pageNum, boolean docModified) {
        pdfView.setCurrentPage(pageNum);
    }

    private void showThumbnailsFragment() {
        ThumbnailsViewFragment mThumbFragment;
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        mThumbFragment = ThumbnailsViewFragment.newInstance(pdfView, this);
        mThumbFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_AppCompat_Light_NoActionBar);
        mThumbFragment.show(fragmentManager, ThumbnailsViewFragment.TAG);
    }

    private void showBookmarksFragment() {
        try {
            long objnum = pdfView.getDoc().getPage(pdfView.getCurrentPage()).getSDFObj().getObjNum();
            UserBookmarkDialogFragment frag = UserBookmarkDialogFragment.newInstance(bookId, pdfView.getCurrentPage(),
                    objnum, userId).setPDFViewCtrl(pdfView).setListener(this);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            frag.show(fragmentManager, UserBookmarkDialogFragment.TAG);
        } catch (PDFNetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUserBookmarkClicked(int pageNum) {
        pdfView.setCurrentPage(pageNum);
    }

    private void createToolManager() {
        mToolManager = new ToolManager(pdfView);
        //Desabilitamos a toolbar do projeto tools;
        mToolManager.setBuiltInPanModeToolbarEnable(false);
        //Desabilitamos o indicador de paginas;
        mToolManager.setBuiltInPageNumberIndicatorVisible(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity) {
            fragmentCommunication = (PdfReaderCommunication) getActivity();
        }
    }

    private void showAnnotations() {
        imvToggleAnnotations.setVisibility(View.VISIBLE);
        imvFreeHand.setVisibility(View.VISIBLE);
        imvListAnnotations.setVisibility(View.VISIBLE);
    }

    private void hideAnnotations() {
        imvToggleAnnotations.setVisibility(View.GONE);
        imvFreeHand.setVisibility(View.GONE);
        imvListAnnotations.setVisibility(View.GONE);
    }

    public void showAnnotationsChangesError() {
        pdfView.closeTool();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.annoation_changes_error)
                .setNegativeButton(R.string.discard, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    closeReader();
                })
                .setPositiveButton(R.string.save, (dialog, which) -> {
                    dialog.dismiss();
                    presenter.sendAnnotationsThenClose(addCommands, modifyCommands, removeCommands);
                });
        builder.create().show();
    }

    @Override
    public void clearCommandsLists() {
        addCommands.clear();
        modifyCommands.clear();
        removeCommands.clear();
    }

    @Override
    public void clearBKPCommands() {
        addCommandsBKP.clear();
        modifyCommandsBKP.clear();
        removeCommandsBKP.clear();
        presenter.discardAnnotationsBackup();
    }

    public boolean hasAnnotationChanges() {
        return addCommands.size() > 0 || modifyCommands.size() > 0 || removeCommands.size() > 0;
    }

    private boolean hasBackupAnnotations() {
        return addCommandsBKP.size() > 0 || modifyCommandsBKP.size() > 0 || removeCommandsBKP.size() > 0;
    }

    /**
     * Este método deve ser chamado quando o usuário tentar fechar o leitor.
     * Ele irá mostrar uma mensagem solicitando ao usuário salvar ou descartar
     * as alterações feitas nas anotações.
     */
    public void willLeave() {
        if (isAnnotationsEnabled && hasAnnotationChanges()) {
            showAnnotationsChangesError();
        } else {
            closeReader();
        }
    }

    @Override
    public void closeReader() {
        if (isAnnotationsEnabled) {
            clearBKPCommands();
            clearCommandsLists();
        }
        fragmentCommunication.closeReader();
    }

    @Override
    public void setActivityMap(HashMap<Integer, List<BookActivityInfo>> activityMap) {
        this.activityMap = activityMap;
    }
}
