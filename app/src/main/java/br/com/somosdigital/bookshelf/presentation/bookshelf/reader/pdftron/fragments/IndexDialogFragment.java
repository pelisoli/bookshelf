//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pdftron.common.PDFNetException;
import com.pdftron.pdf.Action;
import com.pdftron.pdf.Bookmark;
import com.pdftron.pdf.PDFViewCtrl;

import java.util.ArrayList;

import br.com.somosdigital.bookshelf.R;


/**
 * The IndexDialogFragment shows a document outline (bookmarks) that can be
 * used to navigate the document in the PDFViewCtrl.
 */
public class IndexDialogFragment extends DialogFragment {

    public static final String TAG = "INDEXDIALOGFRAGMENT";

    public interface IndexDialogListener {
        void onIndexClicked(Bookmark bookmark);
    }

    private IndexDialogListener mListener;
    private PDFViewCtrl mPDFViewCtrl;

    private ArrayList<Bookmark> mIndexes;
    private IndexesListAdapter mIndexesListAdapter;
    private ListView indexesListView;

    private RelativeLayout mNavigation;
    private TextView mNavigationText;

    private Bookmark mCurrentBookmark;
    private ImageView imvClose;


    public static IndexDialogFragment newInstance(PDFViewCtrl pdfViewCtrl, IndexDialogListener listener) {
        IndexDialogFragment f = new IndexDialogFragment();
        f.setPDFViewCtrl(pdfViewCtrl);
        f.setListener(listener);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void setListener(IndexDialogListener listener){
        this.mListener = listener;
    }

    public void setPDFViewCtrl(PDFViewCtrl pdfViewCtrl) {
        if (pdfViewCtrl == null) {
            throw new NullPointerException("pdfViewCtrl can't be null");
        }
        mPDFViewCtrl = pdfViewCtrl;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.controls_fragment_outline_dialog, null);

        imvClose = view.findViewById(R.id.book_index_imv_close);
        imvClose.setOnClickListener(view1 -> dismissAllowingStateLoss());

        mIndexes = new ArrayList<>();
        ArrayList<Bookmark> bookmarks = getBookmarkList(null);
        if (bookmarks != null) {
            mIndexes.addAll(bookmarks);
        }

        mIndexesListAdapter = new IndexesListAdapter(getActivity(), R.layout.controls_fragment_outline_listview_item, mIndexes);
        indexesListView = view.findViewById(R.id.control_outline_listview);
        indexesListView.setEmptyView(view.findViewById(R.id.control_outline_textview_empty));
        indexesListView.setAdapter(mIndexesListAdapter);
        indexesListView.setOnItemClickListener((parent, view1, position, id) -> {
            try {
                Action action = mIndexes.get(position).getAction();
                if (action != null && action.isValid()) {
                    if (mListener != null) {
                        mListener.onIndexClicked(mIndexes.get(position));
                        dismiss();
                    }
                }
            } catch (PDFNetException e) {
                Toast.makeText(getActivity(), "This bookmark has an invalid action", Toast.LENGTH_SHORT).show();
            }
        });

        // Navigation
        mNavigation = view.findViewById(R.id.control_outline_layout_navigation);
        mNavigation.setVisibility(View.GONE);
        mNavigationText = mNavigation.findViewById(R.id.control_outline_layout_navigation_title);
        try {
            if (mCurrentBookmark != null && mCurrentBookmark.getIndent() > 0) {
                mNavigationText.setText(mCurrentBookmark.getTitle());
                if (mCurrentBookmark.getIndent() <= 0) {
                    mNavigation.setVisibility(View.GONE);
                } else {
                    mNavigation.setVisibility(View.VISIBLE);
                }
            } else {
                mNavigation.setVisibility(View.GONE);
            }
        } catch (PDFNetException e) {
            mNavigation.setVisibility(View.GONE);
        }
        mNavigation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToParentBookmark();
            }
        });

        return view;
    }


    private void navigateToParentBookmark() {
        ArrayList<Bookmark> temp = null;
        try {
            if (mCurrentBookmark != null && mCurrentBookmark.getIndent() > 0) {
                mCurrentBookmark = mCurrentBookmark.getParent();
                temp = getBookmarkList(mCurrentBookmark.getFirstChild());
                mNavigationText.setText(mCurrentBookmark.getTitle());
                if (mCurrentBookmark.getIndent() <= 0) {
                    mNavigation.setVisibility(View.GONE);
                }
            } else {
                temp = getBookmarkList(null);
                mCurrentBookmark = null;
                mNavigation.setVisibility(View.GONE);
            }
        } catch (PDFNetException e) {
            mCurrentBookmark = null;
            temp = null;
        }

        if (temp != null) {
            mIndexes.clear();
            mIndexes.addAll(temp);
            mIndexesListAdapter.notifyDataSetChanged();
        }
    }

    private class IndexesListAdapter extends ArrayAdapter<Bookmark> {

        private Context mContext;
        private int mLayoutResourceId;
        private ArrayList<Bookmark> mBookmarks;

        private ViewHolder mViewHolder;

        public IndexesListAdapter(Context context, int resource, ArrayList<Bookmark> objects) {
            super(context, resource, objects);

            mContext = context;
            mLayoutResourceId = resource;
            mBookmarks = objects;
        }

        @Override
        public int getCount() {
            if (mBookmarks != null) {
                return mBookmarks.size();
            } else {
                return 0;
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(mLayoutResourceId, null);

                mViewHolder = new ViewHolder();
                mViewHolder.bookmarkText = convertView.findViewById(R.id.control_outline_listview_item_textview);
                mViewHolder.bookmarkArrow = convertView.findViewById(R.id.control_outline_listview_item_imageview);

                convertView.setTag(mViewHolder);

            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }

            mViewHolder.bookmarkArrow.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    IndexDialogFragment.this.mCurrentBookmark = mBookmarks.get(position);

                    ArrayList<Bookmark> temp = new ArrayList<Bookmark>();
                    try {
                        temp = getBookmarkList(IndexDialogFragment.this.mCurrentBookmark.getFirstChild());
                    } catch (PDFNetException e) {
                        temp.clear();
                    }
                    if (temp != null) {
                        mBookmarks.clear();
                        mBookmarks.addAll(temp);
                        notifyDataSetChanged();
                        IndexDialogFragment.this.mNavigation.setVisibility(View.VISIBLE);
                        try {
                            IndexDialogFragment.this.mNavigationText.setText(IndexDialogFragment.this.mCurrentBookmark.getTitle());
                        } catch (PDFNetException e) {
//                            com.pdftron.pdf.utils.AnalyticsHandlerAdapter.getInstance().sendException(e);
                        }
                    }
                }
            });

            Bookmark bookmark = mBookmarks.get(position);
            try {
                mViewHolder.bookmarkText.setText(bookmark.getTitle());
                if (bookmark.hasChildren()) {
                    mViewHolder.bookmarkArrow.setVisibility(View.VISIBLE);
                } else {
                    mViewHolder.bookmarkArrow.setVisibility(View.GONE);
                }
            } catch (PDFNetException e) {
//                com.pdftron.pdf.utils.AnalyticsHandlerAdapter.getInstance().sendException(e);
            }

            return convertView;
        }

        private class ViewHolder {
            public TextView bookmarkText;
            public ImageView bookmarkArrow;
        }
    }

    private ArrayList<Bookmark> getBookmarkList(Bookmark firstSibling) {
        ArrayList<Bookmark> bookmarkList = new ArrayList<Bookmark>();

        if (mPDFViewCtrl != null) {
            try {
                Bookmark current;
                if (firstSibling == null) {
                    current = mPDFViewCtrl.getDoc().getFirstBookmark();
                } else {
                    current = firstSibling;
                }

                int numBookmarks = 0;
                while (current.isValid()) {
                    bookmarkList.add(current);
                    current = current.getNext();
                    numBookmarks++;
                }
                if (firstSibling == null && numBookmarks == 1) {
                    ArrayList<Bookmark> bookmarkListNextLevel = new ArrayList<Bookmark>();
                    bookmarkListNextLevel.addAll(getBookmarkList(mPDFViewCtrl.getDoc().getFirstBookmark().getFirstChild()));
                    if (bookmarkListNextLevel.size() > 0) {
                        return bookmarkListNextLevel;
                    }
                }
            } catch (PDFNetException e) {
                bookmarkList.clear();
            }
        }

        return bookmarkList;
    }

}
