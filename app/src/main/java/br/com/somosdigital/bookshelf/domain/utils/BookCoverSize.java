package br.com.somosdigital.bookshelf.domain.utils;

/**
 * Created by pelisoli on 8/18/17.
 */

public class BookCoverSize {

    public static final int COVER_WIDTH = 220;

    public static final int COVER_HEIGHT = 290;

}
