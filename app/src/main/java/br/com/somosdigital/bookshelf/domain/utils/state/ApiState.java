package br.com.somosdigital.bookshelf.domain.utils.state;

import android.content.Context;

import br.com.solucaoadapta.shared.domain.openid.TokenManager;
import br.com.solucaoadapta.shared.domain.services.aps.APSService;
import br.com.solucaoadapta.shared.domain.services.aps.APSServiceGenerator;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorServiceGenerator;
import br.com.somosdigital.bookshelf.service.template.TemplateService;
import br.com.somosdigital.bookshelf.service.template.TemplateServiceGenerator;

/**
 * Created by pelisoli on 10/10/17.
 */

public class ApiState {
    private static TemplateService templateService;

    private static DistribuidorService distribuidorService;

    private static APSService apsService;

    private static TokenManager tokenManager;

    public static DistribuidorService getDistribuidorService(){
        if(distribuidorService == null){
            distribuidorService = new DistribuidorService(DistribuidorServiceGenerator.createService(tokenManager));
        }

        return distribuidorService;
    }

    public static APSService getAPSInstance(){
        if (apsService == null){
            apsService = new APSService
                    (
                            APSServiceGenerator.createService(tokenManager,
                            br.com.solucaoadapta.shared.BuildConfig.APS_URL)
                    );
        }
        return apsService;
    }

    public static TemplateService getTemplateService(Context context){
        if(templateService == null){
            templateService = new TemplateService(TemplateServiceGenerator.createService(context));
        }

        return templateService;
    }

    public static void clear(){

        if(tokenManager != null){
            tokenManager.resetToken();
        }

        apsService = null;
        templateService = null;
        distribuidorService = null;
        tokenManager =  null;
    }

    public static void setTokenManager(TokenManager tokenManager){
        ApiState.tokenManager = tokenManager;
    }

    public static TokenManager getTokenManager() {
        return tokenManager;
    }
}
