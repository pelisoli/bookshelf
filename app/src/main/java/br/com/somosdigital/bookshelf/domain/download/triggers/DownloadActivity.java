package br.com.somosdigital.bookshelf.domain.download.triggers;

import br.com.converge.download.android.DownloadRequest;
import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadCallback;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.ActivityFile;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadActivity {

    private DownloadStatusListener downloadStatusListener;

    private RequestCreator requestCreator;

    private DistribuidorService service;

    private DownloadCallback downloadCallback;

    private CompositeDisposable compositeDisposable;

    private Log log;

    public DownloadActivity(DownloadStatusListener downloadStatusListener,
                            RequestCreator requestCreator,
                            DistribuidorService service,
                            DownloadCallback downloadCallback,
                            CompositeDisposable compositeDisposable,
                            Log log){
        this.downloadStatusListener = downloadStatusListener;
        this.requestCreator = requestCreator;
        this.service = service;
        this.downloadCallback = downloadCallback;
        this.compositeDisposable = compositeDisposable;
        this.log = log;
    }

    public void prepareDownloadActivity(Activity activity) {
        if (activity != null) {

            ActivityFile activityFile = new ActivityFile();
            activityFile.setId(activity.getId());
            activityFile.setKeyCryptography(activity.getKeyCryptography());

            compositeDisposable.add(
                    service.getActivityFile(activityFile)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(newActivityFile -> {
                                        activity.setFilePath(newActivityFile.getFilePath());
                                        activity.setPassword(newActivityFile.getPassword());

                                        downloadActivity(activity);
                                    },

                                    throwable -> {
                                        if(throwable != null && throwable.getMessage() != null) {
                                            log.logError("prepareDownloadActivity Error: "
                                                    + throwable.getMessage());
                                        }

                                        downloadCallback.activityError(activity);
                                    }
                            )
            );
        }
    }

    private void downloadActivity(Activity activity) {
        if (activity != null) {
            DownloadRequest downloadRequest =
                    requestCreator.createDownloadRequest(
                            activity.getFilePath(),
                            activity.getCompactedPath(),
                            downloadStatusListener);

            DownloadInformation.setCurrentDownloadId(requestCreator.getDownloadManager().add(downloadRequest));
        }
    }
}
