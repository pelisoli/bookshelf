package br.com.somosdigital.bookshelf.domain.bookshelf.utils;

import java.util.ArrayList;
import java.util.List;

import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.storage.repository.book.BookSpecification;

/**
 * Created by marcus on 18/10/16.
 */
//TODO REFATORAR TODA ESSA CLASSE
public class BookListUtil {

    private static List<Book> bookList = new ArrayList<>();

    private static boolean addBook = true;

    private static boolean firstLoad = true;

    public static List<Book> diffList(List<Book> databaseList,
                                      List<Book> userList,
                                      List<Book> apiBookList,
                                      List<BookshelfUser> users,
                                      String userId,
                                      BookSpecification bookSpecification) {

        List<Book> excludeList = new ArrayList<>();

        // Rotina de exclusão de livro da banca.
        for (Book userBook : userList) {
            boolean remove = true;

            for (Book apiBook : apiBookList) {
                if (userBook.getId().equals(apiBook.getId())) {
                    remove = false;
                    break;
                }
            }

            if (remove) {
                excludeList.add(userBook);

                if (userBook.getGlobalDownloadStatus() != DownloadStatus.DELETING &&
                        userBook.getGlobalDownloadStatus() != DownloadStatus.CANCELING &&
                        userBook.getGlobalDownloadStatus() != DownloadStatus.NEW) {

                    bookSpecification.remove(userBook);
                }
            }
        }

        if (excludeList.size() > 0) {
            userList.removeAll(excludeList);
        }

        // Pega dados de livros já contidos na base de dados e adiciona a nova lista.
        for (Book apiBook : apiBookList) {
            for (Book dbBook : databaseList) {
                if (BookListUtil.isFirstLoad()
                        && (dbBook.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_ACTIVITY
                        || dbBook.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_OED
                        || dbBook.getGlobalDownloadStatus() == DownloadStatus.DECOMPRESSING)) {
                    dbBook.setGlobalDownloadStatus(DownloadStatus.PAUSED);
                }

                if (apiBook.getId().equals(dbBook.getId())) {
                    updateMergeListBooks(dbBook, apiBook);
                    break;
                }
            }
        }

        // Adiciona a nova lista a lista de livros da banca (referencia de memoria precisa ser mantida)
        for (Book apiBook : apiBookList) {
            for (Book userBook : userList) {
                if (userBook.getId().equals(apiBook.getId())) {
                    updateUserListBooks(apiBook, userBook);
                    addBook = false;
                    break;
                }
            }

            if (addBook) {
                userList.add(apiBook);
            }

            addBook = true;
        }

        return userList;
    }

    public static void updateMergeListBooks(Book bookDatabase, Book apiBook) {
        if (apiBook.getActivity() != null && bookDatabase.getActivity() != null) {
            apiBook.getActivity().setDownloadStatus(bookDatabase.getActivity().getDownloadStatus());
            apiBook.getActivity().setActivityPath(bookDatabase.getActivity().getActivityPath());
            apiBook.getActivity().setCompactedPath(bookDatabase.getActivity().getCompactedPath());
            apiBook.getActivity().setTempPath(bookDatabase.getActivity().getTempPath());
            apiBook.getActivity().setBrkPath(bookDatabase.getActivity().getBrkPath());
        }

        apiBook.setKeyCryptography(bookDatabase.getKeyCryptography());
        apiBook.setEducationalObjects(bookDatabase.getEducationalObjects());
        apiBook.setDownloadStatus(bookDatabase.getDownloadStatus());
        apiBook.setGlobalDownloadStatus(bookDatabase.getGlobalDownloadStatus());
        apiBook.setTempPath(bookDatabase.getTempPath());
        apiBook.setBookPath(bookDatabase.getBookPath());
        apiBook.setCompactedPath(bookDatabase.getCompactedPath());
        apiBook.setBrkPath(bookDatabase.getBrkPath());
        apiBook.setTotalBytesToDownload(bookDatabase.getTotalBytesToDownload());
        apiBook.setCurrentBytesDownloaded(bookDatabase.getCurrentBytesDownloaded());
    }

    private static void updateUserListBooks(Book apiBook, Book userBook) {
        userBook.setBookType(apiBook.getBookType());
        userBook.setTitle(apiBook.getTitle());
        userBook.setCoverPath(apiBook.getCoverPath());
        userBook.setAuthors(apiBook.getAuthors());
        if (apiBook.getAuthors() != null && apiBook.getAuthors().size() > 0) {
            userBook.setAuthorsNames(apiBook.getAuthors().get(0).getName());
        } else {
            userBook.setAuthorsNames("");
        }
        userBook.setCollection(apiBook.getCollection());
        userBook.setGrade(apiBook.getGrade());
        userBook.setSubjectList(apiBook.getSubjectList());
        userBook.setOedLength(apiBook.getOedLength());
        userBook.setExpiredDistribution(apiBook.isExpiredDistribution());
        userBook.setExpires(apiBook.isExpires());
        userBook.setEndDateDistribution(apiBook.getEndDateDistribution());
        userBook.setAllowOedDownload(apiBook.isAllowOedDownload());
        userBook.setPrimaryInfo(apiBook.getPrimaryInfo());
        userBook.setSecondaryInfo(apiBook.getSecondaryInfo());
        userBook.setGlobalDownloadStatus(apiBook.getGlobalDownloadStatus());

        if (userBook.getFile() != null && apiBook.getFile() != null) {
            boolean shouldUpdateBook = apiBook.getFile().getVersion() > userBook.getFile().getVersion();

            if (shouldUpdateBook && userBook.getGlobalDownloadStatus() != DownloadStatus.NEW) {

                if (DownloadInformation.getCurrentBookId().equals(userBook.getId())) {
                    userBook.setUpdate(true);
                } else if (userBook.getGlobalDownloadStatus() == DownloadStatus.SUCCESSFUL
                        || userBook.getGlobalDownloadStatus() == DownloadStatus.PAUSED
                        || userBook.getGlobalDownloadStatus() == DownloadStatus.FAILED
                        || userBook.getGlobalDownloadStatus() == DownloadStatus.ALL_BOOK_SUCCESSFUL
                        || userBook.getGlobalDownloadStatus() == DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL) {
                    userBook.setUpdate(false);
                    userBook.setDownloadStatus(DownloadStatus.UPDATE);
                    userBook.setGlobalDownloadStatus(DownloadStatus.UPDATE);
                }
            }
        }

        if (userBook.getActivity() != null && apiBook.getActivity() != null) {
            boolean shouldUpdateActivity = userBook.getActivity().getVersion() < apiBook.getActivity().getVersion();

            if (shouldUpdateActivity && userBook.getGlobalDownloadStatus() != DownloadStatus.NEW) {
                userBook.setActivity(apiBook.getActivity());
                userBook.getActivity().setDownloadStatus(DownloadStatus.UPDATE);
                userBook.setGlobalDownloadStatus(DownloadStatus.UPDATE);
            }
        }

        userBook.setFile(apiBook.getFile());
        //use fileLength from book model during app execution
        if (apiBook.getFile() != null) {
            userBook.setFileLength(apiBook.getFile().getFileLength());
        }

    }

    public static void firstLoadBooks(List<Book> apiBook) {
        for (Book books : apiBook) {
            books.setFileLength(books.getFile() != null ? books.getFile().getFileLength() : 0);
        }
    }

    public static List<Book> getBookList() {
        return bookList;
    }

    public static void setBookList(List<Book> bookList) {
        BookListUtil.bookList = bookList;
    }

    public static boolean isFirstLoad() {
        return firstLoad;
    }

    public static void setFirstLoad(boolean firstLoad) {
        BookListUtil.firstLoad = firstLoad;
    }
}
