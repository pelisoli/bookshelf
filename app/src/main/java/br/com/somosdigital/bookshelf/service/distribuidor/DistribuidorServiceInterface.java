package br.com.somosdigital.bookshelf.service.distribuidor;

import java.util.List;

import br.com.somosdigital.bookshelf.BuildConfig;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.ActivityFile;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookFile;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Chapter;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by marcus.martins on 05/09/16.
 */
public interface DistribuidorServiceInterface {
    //    Activities
    @POST("api/activities/" + BuildConfig.APS_BOOKSHELF_VERSION + "/activities_files/custom/download")
    Observable<ActivityFile> getActivityFile(@Body ActivityFile activityObject);

    // Books
    @GET("api/activities/" + BuildConfig.APS_BOOKSHELF_VERSION + "/distributions/compatibility_bookshelf")
    Observable<List<Book>> getBooks(@Query("device_id") String deviceId,
                                    @Query("screen_density") String screenDensity,
                                    @Query("file_type") String fileType);

    @POST("api/activities/" + BuildConfig.APS_BOOKSHELF_VERSION + "/books/custom/download")
    Observable<BookFile> getBookFile(@Body BookFile bookObject);

    // Educational Objects
    @POST("api/activities/" + BuildConfig.APS_BOOKSHELF_VERSION + "/educational_objects/custom/download")
    Observable<Oed> getOedFile(@Body Oed oedObject);

    @GET("api/activities/" + BuildConfig.APS_BOOKSHELF_VERSION + "/chapters")
    Observable<List<Chapter>> getChapters(@Query("q") String query,
                                          @Query("m") String mode,
                                          @Query("s") String sort,
                                          @Query("l") String limit);

    @Headers("Content-Type: text/xml; charset=utf-8")
    @GET("code/annotations")
    Observable<Response<ResponseBody>> getXFDFFile(@Query("did") String bookId);

    @Headers("Content-Type: text/xml; charset=utf-8")
    @POST("code/annotations")
    Observable<Response<ResponseBody>> sendXFDFFile(@Query("did") String bookId,
                                                    @Body RequestBody body);
}
