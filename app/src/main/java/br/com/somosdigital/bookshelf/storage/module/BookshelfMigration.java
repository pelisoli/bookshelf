package br.com.somosdigital.bookshelf.storage.module;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by pelisoli on 10/18/17.
 */

public class BookshelfMigration implements RealmMigration{
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        Log.e("DATABASE", "oldVersion -> " + oldVersion);
        Log.e("DATABASE", "newVersion -> " + newVersion);

        final RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            RealmObjectSchema oedSchema = schema.get("Oed");

            if (oedSchema != null) {
                if (oedSchema.hasField("name")) {
                    oedSchema.renameField("name", "title");
                }
            }

            oldVersion++;
        }

        //Pelos testes realizados devido a falhas de migration em produção, essa versão nunca existiu em produção.
        // Pulamos para a 3
        if (oldVersion == 2) {
            oldVersion++;
        }

        if (oldVersion == 3) {
            RealmObjectSchema bookSchema = schema.get("Book");

            if (bookSchema != null) {
                bookSchema
                        .addField("primaryInfo", String.class)
                        .addField("secondaryInfo", String.class)
                        .addField("authorsNames", String.class)
                        .addField("bookType", String.class)
                        .removeField("authors");
            }

            schema.remove("Author");

            RealmObjectSchema oedSchema = schema.get("Oed");

            if (oedSchema != null) {
                oedSchema
                        .addField("pages", String.class);

                if (oedSchema.hasField("page")) {
                    oedSchema.removeField("page");
                }
            }

            oldVersion++;
        }
    }
}
