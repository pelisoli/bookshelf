package br.com.somosdigital.bookshelf.domain.cryptography;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import br.com.converge.encryption.android.RSA;
import br.com.somosdigital.bookshelf.domain.utils.DrmClient;
import timber.log.Timber;


/**
 * Created by pelisoli on 09/06/15.
 */
public class CryptographyManager {
    public static String decriptFromFile(String path, Context context) {
        String encryptionKey = "";

        if(path != null && !path.isEmpty()) {
            File file = new File(path);

            try {
                if (file.exists()) {
                    int size = (int) file.length();
                    byte[] bytes = new byte[size];

                    if (size > 0) {
                        BufferedInputStream buf =
                                new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes, 0, bytes.length);
                        buf.close();

                        //TODO pass preference preference name. Null value is temporary
                        DrmClient drmClient = new DrmClient(context, null);

                        encryptionKey =
                                new RSA().decrypt(new String(bytes), drmClient
                                        .getKeys().getPrivateKey());

                    }
                }
            } catch (Exception e) {
                if (e != null && e.getStackTrace() != null) {
                    Timber.e( Log.getStackTraceString(e));
                }
            }
        }

        return encryptionKey;
    }

    public static String decriptyFromKey(String keyCriptography, String preferenceName, Context context) {
        String encryptionKey = "";

        if(keyCriptography != null && !keyCriptography.isEmpty()) {

            try {

                //TODO pass preference preference name. Null value is temporary
                DrmClient drmClient = new DrmClient(context, preferenceName);

                encryptionKey =
                        new RSA().decrypt(keyCriptography, drmClient
                                .getKeys().getPrivateKey());


            } catch (Exception e) {
                if (e != null && e.getStackTrace() != null) {
                    Timber.e( Log.getStackTraceString(e));
                }
            }
        }

        return encryptionKey;
    }

    public static boolean createKeyCriptographyFile(String path, String key) {
        boolean status = false;

        if (path != null && !path.isEmpty()) {
            File file =
                    new File(path);

            FileOutputStream fos = null;

            try {
                if (!file.getParentFile().exists()
                        && !file.getParentFile().mkdirs()) {
                    return true;
                }

                if (key != null) {
                    fos = new FileOutputStream(file);
                    fos.write(key.getBytes());
                    fos.close();

                    status = true;
                }

            } catch (Exception e) {
                if (e != null && e.getStackTrace() != null) {
                    Timber.e(Log.getStackTraceString(e));
                }

                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e1) {
                        if (e1 != null && e1.getStackTrace() != null) {
                            Timber.e( Log.getStackTraceString(e1));
                        }
                    }
                }
            }
        }

        return status;
    }

    public static String getPublicKey(Context context, String preferenceName){
        DrmClient drmClient = new DrmClient(context, preferenceName);

        return drmClient.getKeys().getPublicKey();
    }
}
