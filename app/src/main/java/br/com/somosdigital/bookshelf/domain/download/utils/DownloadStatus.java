package br.com.somosdigital.bookshelf.domain.download.utils;


import br.com.somosdigital.bookshelf.R;

/**
 * Created by pelisoli on 26/05/15.
 */
public enum DownloadStatus {

    NEW(Ids.NEW, R.drawable.ic_download),
    PAUSED(Ids.PAUSED, R.drawable.ic_sm_download_pausa),
    CANCELING(Ids.CANCELING, R.drawable.ic_sm_download_cancelado),
    FAILED(Ids.FAILED, R.drawable.ic_sm_erro_download),
    DELETING(Ids.DELETING, R.drawable.ic_sm_deletar_livro),
    UPDATE(Ids.UPDATE, R.drawable.ic_sm_livro_atualizar),
    DECOMPRESSING(Ids.DECOMPRESSING, R.drawable.ic_sm_extrair_arquivo),
    DOWNLOADING_BOOK_ACTIVITY(Ids.DOWNLOADING_BOOK_ACTICITY, R.drawable.ic_sm_baixando_livro),
    DOWNLOADING_BOOK_OED(Ids.DOWNLOADING_BOOK_OED, R.drawable.ic_sm_baixando_oed),
    BOOK_ACTIVITY_SUCCESSFUL(Ids.BOOK_ACTIVITY_SUCCESSFUL, R.drawable.ic_sm_baixando_livro),
    ALL_BOOK_SUCCESSFUL(Ids.ALL_BOOK_SUCCESSFUL, R.drawable.ic_sm_download_completo),
    SUCCESSFUL(Ids.SUCCESSFUL, 0);

    protected int id;
    protected int image;

    DownloadStatus(int id, int image) {
        this.id = id;
        this.image = image;
    }

    public static class Ids {
        public static final int NEW = 1;
        public static final int PAUSED = 2;
        public static final int CANCELING = 3;
        public static final int FAILED = 4;
        public static final int DELETING = 5;
        public static final int UPDATE = 6;
        public static final int DECOMPRESSING = 7;
        public static final int DOWNLOADING_BOOK_ACTICITY = 8;
        public static final int DOWNLOADING_BOOK_OED = 9;
        public static final int BOOK_ACTIVITY_SUCCESSFUL = 10;
        public static final int ALL_BOOK_SUCCESSFUL = 11;
        public static final int SUCCESSFUL = 12;
    }

    public int getImage() {
        return image;
    }

    public boolean hasImage() {
        return image != 0;
    }

    public int getId() {
        return id;
    }

    public static DownloadStatus fromInt(int id) {
        if (NEW.id == id) {
            return NEW;
        } else if (PAUSED.id == id) {
            return PAUSED;
        } else if (CANCELING.id == id) {
            return CANCELING;
        } else if (FAILED.id == id) {
            return FAILED;
        } else if (DELETING.id == id) {
            return DELETING;
        } else if (UPDATE.id == id) {
            return UPDATE;
        } else if (DECOMPRESSING.id == id) {
            return DECOMPRESSING;
        } else if (DOWNLOADING_BOOK_ACTIVITY.id == id) {
            return DOWNLOADING_BOOK_ACTIVITY;
        } else if (DOWNLOADING_BOOK_OED.id == id) {
            return DOWNLOADING_BOOK_OED;
        } else if (BOOK_ACTIVITY_SUCCESSFUL.id == id) {
            return BOOK_ACTIVITY_SUCCESSFUL;
        } else if (ALL_BOOK_SUCCESSFUL.id == id) {
            return ALL_BOOK_SUCCESSFUL;
        } else if (SUCCESSFUL.id == id) {
            return SUCCESSFUL;
        } else {
            return null;
        }
    }

}