package br.com.somosdigital.bookshelf.storage.repository.oed;

import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;

/**
 * Created by pelisoli on 11/4/17.
 */

public interface OedSpecification extends Repository<Oed> {

}
