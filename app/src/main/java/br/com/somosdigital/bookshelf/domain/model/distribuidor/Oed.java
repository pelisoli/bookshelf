package br.com.somosdigital.bookshelf.domain.model.distribuidor;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by marcus.martins on 06/09/16.
 */
public class Oed extends RealmObject{
    @PrimaryKey
    @Expose()
    @SerializedName(value = "educational_object_id", alternate = {"id"})
    private String id;

    @Expose(serialize = false)
    private long fileLength;

    @Expose(serialize = false)
    private String pages;

    @Expose(serialize = false)
    private String title;

    @Ignore
    @Expose(serialize = false)
    private String filePath;

    @Ignore
    @Expose(serialize = false)
    private String password;

    @Expose()
    @Ignore
    private String keyCryptography;

    @Expose()
    @Ignore
    private String bookId;

    @Expose(serialize = false)
    private int downloadStatus;

    @Expose(serialize = false)
    private String oedPath;

    @Expose(serialize = false)
    private String compactedPath;

    @Expose(serialize = false)
    private String tempPath;

    @Expose(serialize = false)
    private String brkPath;

    @Expose(serialize = false)
    private String linkPreview;

    @Ignore
    @Expose()
    private String fileType = "pdf";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeyCryptography() {
        return keyCryptography;
    }

    public void setKeyCryptography(String keyCryptography) {
        this.keyCryptography = keyCryptography;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public DownloadStatus getDownloadStatus() {
        return DownloadStatus.fromInt(this.downloadStatus);
    }

    public void setDownloadStatus(DownloadStatus downloadStatus) {
        this.downloadStatus = downloadStatus.getId();
    }

    public String getOedPath() {
        return oedPath;
    }

    public void setOedPath(String oedPath) {
        this.oedPath = oedPath;
    }

    public String getCompactedPath() {
        return compactedPath;
    }

    public void setCompactedPath(String compactedPath) {
        this.compactedPath = compactedPath;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getBrkPath() {
        return brkPath;
    }

    public void setBrkPath(String brkPath) {
        this.brkPath = brkPath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkPreview() {
        return linkPreview;
    }

    public void setLinkPreview(String linkPreview) {
        this.linkPreview = linkPreview;
    }
}
