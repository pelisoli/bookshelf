package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by marcus.martins on 06/09/16.
 */
public class Collection extends RealmObject {
    @Expose()
    @PrimaryKey
    private String id;

    @Expose()
    private String name;

    @Expose()
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
