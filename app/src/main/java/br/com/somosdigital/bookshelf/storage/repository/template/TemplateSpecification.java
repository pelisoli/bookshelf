package br.com.somosdigital.bookshelf.storage.repository.template;


import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;

/**
 * Created by pelisoli on 16/09/16.
 */
public interface TemplateSpecification extends Repository<Template> {

    Template getLastTemplate();

    void closeRealm();

    void openRealm();
}
