package br.com.somosdigital.bookshelf.presentation.bookshelf.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.domain.openid.TokenManager;
import br.com.solucaoadapta.shared.presentation.dialog.information.InformationDialog;
import br.com.somosdigital.bookshelf.BuildConfig;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.activity.ActivityInfo;
import br.com.somosdigital.bookshelf.domain.activity.AdaptaActivity;
import br.com.somosdigital.bookshelf.domain.activity.AdaptaActivityManager;
import br.com.somosdigital.bookshelf.domain.utils.state.ApiState;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.implementation.ISendActivity;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.implementation.ReaderActivityCallbackImpl;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderObject;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderTemplate;

/**
 * Created by marcus on 28/10/16.
 */

public class BookActivityFragment extends Fragment
        implements BookActivityContract.View, ISendActivity, ReaderActivity {

    private WebView readerView;

    private ReaderTemplate jsCallback;

    private Object objectCallback;

    private BookActivityPresenter presenter;

    private String activityId;

    private String bookId;

    private String activityType;

    private boolean authorship;

    private String contentId;

    private String bookPage = "";

    private Log log;

    private BookActivityCommunication bookActivityCommunication;

    private String activityPayload;

    private String statementPayload;

    private String userId;

    private boolean isTeacher;

    private boolean canSendActivty;

    //TODO Put all parameters in an object
    public static BookActivityFragment newInstance(String activityId,
                                                   String bookId,
                                                   String activityType,
                                                   String preferenceName,
                                                   String contentId,
                                                   boolean authorship,
                                                   String bookPage,
                                                   String userId,
                                                   boolean isTeacher,
                                                   boolean canSendActivity,
                                                   Log log) {
        Bundle bundle = new Bundle();
        bundle.putString("activityId", activityId);
        bundle.putString("bookId", bookId);
        bundle.putString("activityType", activityType);
        bundle.putString("preferenceName", preferenceName);
        bundle.putString("contentId", contentId);
        bundle.putBoolean("authorship", authorship);
        bundle.putString("bookPage", bookPage);
        bundle.putString("userId", userId);
        bundle.putBoolean("isTeacher", isTeacher);
        bundle.putBoolean("canSendActivity", canSendActivity);
        bundle.putParcelable("log", log);

        BookActivityFragment fragment = new BookActivityFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bookactivity_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WebView.setWebContentsDebuggingEnabled(br.com.solucaoadapta.shared.BuildConfig.WEBVIEW_DEBUG);

        getBasicInfo(getArguments());

        readerView = view.findViewById(R.id.activity_view);
        readerView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        readerView.getSettings().setJavaScriptEnabled(true);

        readerView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }
        });

        readerView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });

        objectCallback = getObjectCallback();

        //Callbacks
        this.jsCallback = new ReaderActivityCallbackImpl(this, this);

        // assign callback to webview
        readerView.addJavascriptInterface(jsCallback, "NativeInterface");

        presenter = new BookActivityPresenter(log, isTeacher, canSendActivty, ApiState.getAPSInstance());
        presenter.attachView(this);
        presenter.prepareActivity(getAdaptaActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            bookActivityCommunication = (BookActivityCommunication) getActivity();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if (presenter != null) {
            presenter.detachView();
        }

        if (readerView != null) {
            readerView.destroy();
        }
    }

    public ReaderObject getObjectCallback(){
        return new ReaderObject() {
            @Override
            public void onObjectCalled(String oedID, String objectPath) {

            }

            @Override
            public void onActivityCalled(String activityId, String activityPath, String activityType) {

            }

            @Override
            public void onErrorToOpenObject(){

            }

            @Override
            public void onOpenMedia(String mediaPath) {
            }
        };
    }

    public static void setTokenManager(TokenManager tokenManager){
        ApiState.setTokenManager(tokenManager);
    }

    @Override
    public void loadActivity(String finalTemplateUrl) {
        readerView.loadUrl(finalTemplateUrl);
    }

    public void getBasicInfo(Bundle bundle) {
        if (bundle != null) {
            activityId = bundle.getString("activityId");
            bookId = bundle.getString("bookId", "");
            activityType = bundle.getString("activityType");
            bookPage = bundle.getString("bookPage");
            contentId = bundle.getString("contentId");
            authorship = bundle.getBoolean("authorship");
            log = bundle.getParcelable("log");
            userId = bundle.getString("userId");
            isTeacher = bundle.getBoolean("isTeacher");
            canSendActivty = bundle.getBoolean("canSendActivity");
        }
    }

    private ActivityInfo getActivityInfo() {
        return new ActivityInfo(bookId, activityType, activityId, contentId, authorship, bookPage);
    }

    private AdaptaActivity getAdaptaActivity() {
        return  new AdaptaActivityManager(getActivityInfo(),
                br.com.solucaoadapta.shared.BuildConfig.APS_URL,
                br.com.solucaoadapta.shared.BuildConfig.APS_VERSION,
                BuildConfig.TEMPLATE_VERSION,
                BuildConfig.WEB_ACTIVITIES_URL);
    }


    @Override
    public void showNotificationError() {
        InformationDialog dialog = InformationDialog.newInstance(getString(R.string.notification_send_activity_error));
        dialog.setCallback(() -> bookActivityCommunication.closeParentActivity());
        dialog.show(getFragmentManager(), InformationDialog.TAG);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, InformationDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void hideSendActivityProgress() {
        Fragment loadingFragment = getFragmentManager().findFragmentByTag(InformationDialog.TAG);
        if (loadingFragment != null){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.remove(loadingFragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void showSendActivityError() {
        InformationDialog dialog = InformationDialog.newInstance(getString(R.string.sent_activity_error));

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, InformationDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void showSendViewSuccess() {
        InformationDialog informationDialog = InformationDialog.newInstance(getString(R.string.success),
                getString(R.string.activity_draft_success));
        informationDialog.setCancelable(false);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(informationDialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void showSendError() {
        InformationDialog informationDialog
                = InformationDialog.newInstance(getString(R.string.attention), getString(R.string.activity_draft_error));

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(informationDialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void showSendActivitySuccess() {
        InformationDialog dialog = InformationDialog.newInstance(getString(R.string.success_sent_activity));
        dialog.setCancelable(false);
        dialog.setCallback(() -> bookActivityCommunication.closeParentActivity());

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialog, InformationDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void showServerMaintenanceError() {
        InformationDialog informationDialog = InformationDialog
                .newInstance(getString(R.string.error_503));

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(informationDialog, InformationDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void sendActivity(String activity, String statement) {
        this.activityPayload =  activity;
        this.statementPayload = statement;

        bookActivityCommunication.selectStudents();
    }


    @Override
    public void saveActivity(String statements) {
        presenter.saveActivity(statements, true);
    }

    @Override
    public String onGetClassInfo() {
        return presenter.getClassInfo
                (
                        objectCallback,
                        getAdaptaActivity(),
                        ApiState.getTokenManager() != null ? ApiState.getTokenManager().getAccessToken() : "",
                        userId,
                        isTeacher
                );
    }
}