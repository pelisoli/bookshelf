package br.com.somosdigital.bookshelf.storage.repository.template;


import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import br.com.somosdigital.bookshelf.storage.module.BookshelfDatabase;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by pelisoli on 16/09/16.
 */
public class TemplateRepository implements TemplateSpecification {
    private Realm realm;

    public TemplateRepository() {
    }

    @Override
    public void add(Template item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<Template> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(Template item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<Template> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(Template item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realm1 -> item.deleteFromRealm());
        }
    }

    private boolean checkRealmInstance(Realm realm){
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }

    @Override
    public Template getLastTemplate() {
        Template result = null;

        if (checkRealmInstance(realm)) {
            RealmResults<Template> templates = realm.where(Template.class).findAll();

            if(templates != null && templates.size() > 0) {
                result = templates.last();
            }
        }

        return result;
    }

    @Override
    public void closeRealm() {
        if(realm != null && !realm.isClosed()){
            realm.close();
            realm = null;
        }
    }

    @Override
    public void openRealm() {
        if (realm == null) {
            realm = Realm.getInstance(BookshelfDatabase.getRealmConfig());
        }
    }
}
