package br.com.somosdigital.bookshelf.domain.model.error;

/**
 * Created by jgsmanaroulas on 15/03/17.
 */
public class CustomErrorCodes {

    public static final String DISTRIBUIDOR_DEVICE_LIMIT_REACHED = "A2";

    public static final String DISTRIBUIDOR_DEVICE_BLOCKED = "J2";

    public static final String BOOK_EXPIRED = "BKS001";

}
