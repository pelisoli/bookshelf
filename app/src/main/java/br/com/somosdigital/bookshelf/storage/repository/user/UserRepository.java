package br.com.somosdigital.bookshelf.storage.repository.user;

import java.util.List;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by pelisoli on 14/09/16.
 */
public class UserRepository implements UserSpecification {

    Realm realm;

    public UserRepository(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void add(BookshelfUser item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<BookshelfUser> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(BookshelfUser item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<BookshelfUser> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(BookshelfUser item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> item.deleteFromRealm());
        }
    }

    @Override
    public RealmList<BookshelfUser> getUsers() {
        RealmList<BookshelfUser> result = null;

        if (checkRealmInstance(realm)) {
            if (realm.where(BookshelfUser.class).findAll().size() > 0) {
                List<BookshelfUser> list = realm.copyFromRealm(realm.where(BookshelfUser.class).findAll());

                result = new RealmList<>();
                result.addAll(list);
            }
        }

        return result;
    }

    public BookshelfUser getById(String id) {
        BookshelfUser result = null;

        if (checkRealmInstance(realm) && id != null && !id.equals("")) {
            if (realm.where(BookshelfUser.class).equalTo("id", id).findFirst() !=  null) {
                result = realm.copyFromRealm(realm.where(BookshelfUser.class).equalTo("id", id).findFirst());
            }
        }

        return result;
    }

    private boolean checkRealmInstance(Realm realm){
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }
}
