package br.com.somosdigital.bookshelf.domain.download.listeners;

import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.storage.runtime.AppInfo;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadUnzip;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import br.com.somosdigital.bookshelf.storage.repository.template.TemplateSpecification;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pelisoli on 13/02/17.
 */
public class DownloadTemplateListener implements DownloadStatusListener {

    private Template template;

    private TemplateSpecification templateSpecification;

    private DownloadUnzip downloadUnzip;

    private Log log;

    public DownloadTemplateListener(Template template, TemplateSpecification templateSpecification, DownloadUnzip downloadUnzip, Log log) {
        this.template = template;
        this.templateSpecification = templateSpecification;
        this.downloadUnzip = downloadUnzip;
        this.log = log;
    }

    @Override
    public void onDownloadComplete(int id) {
        log.logDebug("Template download was completed");

        downloadUnzip
                .unzipTemplate(template)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(result -> {
                            if(result){
                                AppInfo.getInstance().setTemplateVersion(template.geteTag());
                                templateSpecification.openRealm();
                                templateSpecification.add(template);
                                templateSpecification.closeRealm();
                            }
                        },

                        throwable -> {
                            log.logError("onDownloadComplete - Template - " + throwable.getMessage());
                        });
    }

    @Override
    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
        log.logError("Template download was failed - " + errorMessage);
    }

    @Override
    public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {
    }

    @Override
    public void onPause(int id, long downloadedBytes) {
        log.logInfo("Template download was paused");
    }

    @Override
    public void onDownloadCanceled(int id) {
        log.logInfo("Template download was canceled");
    }
}