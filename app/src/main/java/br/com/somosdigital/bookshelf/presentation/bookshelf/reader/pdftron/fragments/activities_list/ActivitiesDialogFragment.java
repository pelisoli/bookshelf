package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.activities_list;

import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderCommunication;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.adapter.adapter.ActivitiesListAdapter;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;

public class ActivitiesDialogFragment extends DialogFragment
        implements ActivitiesDialogContract.View, ActivitiesListAdapter.IClickListener {

    private ActivitiesDialogPresenter presenter;

    private ActivitiesDialogFragmentListener mListener;

    private ArrayList<BookActivityInfo> mActivities = new ArrayList<>();

    private ActivitiesListAdapter mActivitiesListAdapter;

    private RecyclerView mRecyclerActivities;

    private LinearLayout linearSendActivities;

    private TextView fragmentTitle;

    boolean canSendActivities;

    private PdfReaderCommunication pdfReaderCommunication;

    private boolean isMultiSelection;

    private ArrayList<BookActivityInfo> selectedActivities;

    public static final String BUNDLE_ACTIVITIES = "activities";

    public static final String TAG = "ACTIVITIESDIALOGFRAGMENT";

    public void setPdfReaderCommunication(PdfReaderCommunication pdfReaderCommunication) {
        this.pdfReaderCommunication = pdfReaderCommunication;
    }

    public interface ActivitiesDialogFragmentListener {
        void onActivitySelected(BookActivityInfo bookActivityInfo);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static ActivitiesDialogFragment newInstance(ActivitiesDialogFragmentListener listener,
                                                       PdfReaderCommunication communication) {
        ActivitiesDialogFragment f = new ActivitiesDialogFragment();
        f.setListener(listener);
        f.setPdfReaderCommunication(communication);
        return f;
    }

    public void setListener(ActivitiesDialogFragmentListener listener) {
        this.mListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.book_activites_layout, null);

        presenter = new ActivitiesDialogPresenter();
        presenter.attachView(this);
        setupViews(view);

        mActivities = getArguments().getParcelableArrayList("activities");
        canSendActivities = getArguments().getBoolean("canSendActivities");
        presenter.orderActivitiesByName(mActivities);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (presenter != null) {
            presenter.detachView();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        dismissAllowingStateLoss();
    }

    private void setupViews(View view) {
        ImageView imvClose = view.findViewById(R.id.book_activities_imv_close);
        Button btnSendHome = view.findViewById(R.id.book_activities_btn_send_home);

        mRecyclerActivities = view.findViewById(R.id.book_activities_recycler);
        mRecyclerActivities.setHasFixedSize(true);
        mRecyclerActivities.setLayoutManager(new LinearLayoutManager(getContext()));
        linearSendActivities = view.findViewById(R.id.book_activities_send_layout);
        imvClose.setOnClickListener(view1 -> dismissAllowingStateLoss());
        fragmentTitle = view.findViewById(R.id.book_activities_title);

        btnSendHome.setOnClickListener(l -> sendActivities());
    }

    public void sendActivities() {
        pdfReaderCommunication.sendActivities(selectedActivities);
    }

    @Override
    public void showServerMaintenanceError() {

    }

    @Override
    public void onItemClick(int position) {
        onActivityClicked(mActivities.get(position));
    }

    @Override
    public void onItemLongClick(int position) {
        if (!isMultiSelection()) {
            onSelectionEnabled();
        }
        onActivityClicked(mActivities.get(position));
    }

    public void onActivityClicked(BookActivityInfo bookActivityInfo) {
        if (isMultiSelection) {
            if (selectedActivities.contains(bookActivityInfo)) {
                bookActivityInfo.setSelected(false);
                selectedActivities.remove(bookActivityInfo);
            } else {
                bookActivityInfo.setSelected(true);
                selectedActivities.add(bookActivityInfo);
            }
            if (selectedActivities.isEmpty()) {
                isMultiSelection = false;
                onSelectionDisabled();
            }
            onMultiSelectionChanged(selectedActivities);
        } else {
            onActivitySelected(bookActivityInfo);
        }
    }

    @Override
    public void showActivities(ArrayList<BookActivityInfo> activities) {
        mActivities = activities;
        mActivitiesListAdapter = new ActivitiesListAdapter(getActivity(), activities, canSendActivities, this);
        mRecyclerActivities.setAdapter(mActivitiesListAdapter);
    }

    public void onActivitySelected(BookActivityInfo activityInfo) {
        if (activityInfo != null) {
            if (mListener != null) {
                mListener.onActivitySelected(activityInfo);
                dismissAllowingStateLoss();
            }
        }
    }

    public void onMultiSelectionChanged(ArrayList<BookActivityInfo> bookActivities) {
        String title;
        if (bookActivities.size() > 0) {
            title = String.format(getString(R.string.book_activities_page_activities_selection), bookActivities.size());
        } else {
            title = getString(R.string.book_activities_page_activities);
        }
        fragmentTitle.setText(title);
        mActivitiesListAdapter.notifyDataSetChanged();
    }

    public void onSelectionDisabled() {
        linearSendActivities.setVisibility(View.GONE);
    }

    private void onSelectionEnabled() {
        isMultiSelection = true;
        selectedActivities = new ArrayList<>();
        linearSendActivities.setVisibility(View.VISIBLE);
    }

    public boolean isMultiSelection() {
        return isMultiSelection;
    }
}
