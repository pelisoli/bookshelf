package br.com.somosdigital.bookshelf.domain.download.utils;

import br.com.converge.download.android.DownloadRequest;
import br.com.converge.download.android.DownloadStatusListener;
import br.com.converge.download.android.ThinDownloadManager;

/**
 * Created by marcus on 17/04/17.
 */

public interface RequestCreator {

    ThinDownloadManager getDownloadManager();

    void setDownloadManager(ThinDownloadManager downloadManager);

    DownloadRequest createDownloadRequest(String downloadPath,
                                          String destinationPath,
                                          DownloadStatusListener listener);

    DownloadRequest createNewDownloadRequest(String downloadPath,
                                             String destinationPath,
                                             DownloadStatusListener listener);

    DownloadRequest createEtagRequest(String downloadPath,
                                      String destinationPath,
                                      String eTag,
                                      DownloadStatusListener listener);

}
