package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;

/**
 * Created by pelisoli on 12/6/17.
 */
public class ActivityFile {

    @Expose()
    private String id;

    @Expose()
    private String keyCryptography;

    @Expose(serialize = false)
    private String filePath;

    @Expose(serialize = false)
    private String password;

    @Expose(serialize = false)
    private long fileLength;

    @Expose(serialize = false)
    private int version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeyCryptography() {
        return keyCryptography;
    }

    public void setKeyCryptography(String keyCryptography) {
        this.keyCryptography = keyCryptography;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
