package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import br.com.converge.encryption.android.RSA;
import br.com.somosdigital.bookshelf.domain.utils.DrmClient;
import timber.log.Timber;

/**
 * Created by pelisoli on 04/09/15.
 */
public class OedManagerAndroid {

    private Context context;

    private String preferenceName;

    public OedManagerAndroid(Context context, String preferenceName) {
        this.context = context;
        this.preferenceName = preferenceName;
    }

    public String getKey(String filePath) {
        try {
            File file = new File(filePath);

            if (file.exists()) {
                int size = (int) file.length();
                byte[] bytes = new byte[size];

                if (size > 0) {
                    BufferedInputStream buf =
                            new BufferedInputStream(new FileInputStream(file));
                    buf.read(bytes, 0, bytes.length);
                    buf.close();

                    DrmClient drmClient = new DrmClient(context, preferenceName);

                    String encryptionKey =
                            new RSA().decrypt(new String(bytes), drmClient
                                    .getKeys().getPrivateKey());

                    return encryptionKey.substring(0, 16);
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            if (e != null && e.getStackTrace() != null) {
                Timber.e(Log.getStackTraceString(e));
            }
            return "";
        }
    }
}
