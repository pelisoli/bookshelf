package br.com.somosdigital.bookshelf.presentation.bookshelf.books;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import br.com.converge.download.android.DownloadRequest;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.domain.services.aps.Headers;
import br.com.solucaoadapta.shared.domain.utils.DateUtil;
import br.com.solucaoadapta.shared.presentation.base.BasePresenter;
import br.com.solucaoadapta.shared.storage.runtime.AppRunningMode;
import br.com.solucaoadapta.shared.storage.runtime.ClassStatusManager;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BookListUtil;
import br.com.somosdigital.bookshelf.domain.download.listeners.DownloadTemplateListener;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadUnzip;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Subject;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import br.com.somosdigital.bookshelf.domain.model.error.CustomErrorCodes;
import br.com.somosdigital.bookshelf.domain.utils.state.BookshelfLibraryState;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import br.com.somosdigital.bookshelf.service.template.TemplateService;
import br.com.somosdigital.bookshelf.storage.module.BookshelfDatabase;
import br.com.somosdigital.bookshelf.storage.repository.book.BookRepository;
import br.com.somosdigital.bookshelf.storage.repository.book.BookSpecification;
import br.com.somosdigital.bookshelf.storage.repository.template.TemplateSpecification;
import br.com.somosdigital.bookshelf.storage.repository.user.UserRepository;
import br.com.somosdigital.bookshelf.storage.repository.user.UserSpecification;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by marcus on 19/10/16.
 */
public class BooksFragmentPresenter extends BasePresenter<BooksFragmentContract.View>
        implements BooksFragmentContract.Presenter {

    private DistribuidorService distribuidorService;

    private DownloadUnzip downloadUnzip;

    private Log log;

    public BooksFragmentPresenter
            (
                    DistribuidorService distribuidorService,
                    DownloadUnzip downloadUnzip,
                    Log log
            ) {

        this.distribuidorService = distribuidorService;
        this.downloadUnzip = downloadUnzip;
        this.log = log;
    }

    @Override
    public BookshelfUser loadUser(String userId) {
        if (BookshelfLibraryState.getBookshelfUser() == null) {
            Realm realm = Realm.getInstance(BookshelfDatabase.getRealmConfig());
            UserSpecification userSpecification = new UserRepository(realm);

            BookshelfUser user = userSpecification.getById(userId);

            if (user == null) {
                user = new BookshelfUser();
                user.setId(userId);

                userSpecification.update(user);
            }

            BookshelfLibraryState.setBookshelfUser(user);
            realm.close();
        }

        return BookshelfLibraryState.getBookshelfUser();
    }

    @Override
    public void loadBooks(BookshelfUser bookshelfUser, String deviceId, String screenDensity) {
        if (distribuidorService != null) {
            Realm realm = Realm.getInstance(BookshelfDatabase.getRealmConfig());

            BookSpecification bookSpecification = new BookRepository(realm);
            UserSpecification userSpecification = new UserRepository(realm);

            addSubscription(
                    distribuidorService
                            .getBooks(deviceId, screenDensity, "pdf")
                            .subscribeOn(Schedulers.io())
                            .doOnNext(books -> {
                                if (books != null) {
                                    for (Book book : books) {
                                        book.setGlobalDownloadStatus(DownloadStatus.NEW);
                                        if (book.getAuthors() != null && book.getAuthors().size() > 0) {
                                            book.setAuthorsNames(book.getAuthors().get(0).getName());
                                        } else {
                                            book.setAuthorsNames("");
                                        }
                                    }

                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(booksFromService -> {
                                        List<Book> bookList = bookSpecification.getBooks();

                                        if (bookList == null || bookList.size() <= 0) {
                                            BookListUtil.firstLoadBooks(booksFromService);
                                            bookshelfUser.setBooks(new RealmList<>(booksFromService.toArray(new Book[booksFromService.size()])));
                                        } else {
                                            if (bookshelfUser.getBooks() == null) {
                                                bookshelfUser.setBooks(new RealmList<>());
                                            }

                                            List<Book> newList = BookListUtil.diffList
                                                    (
                                                            bookList,
                                                            bookshelfUser.getBooks(),
                                                            booksFromService,
                                                            userSpecification.getUsers(),
                                                            bookshelfUser.getId(),
                                                            bookSpecification
                                                    );

                                            bookshelfUser.setBooks(new RealmList<>(newList.toArray(new Book[newList.size()])));
                                        }

                                        userSpecification.update(bookshelfUser);
                                        realm.close();

                                        BookListUtil.setFirstLoad(false);

                                        List<Book> books = bookshelfUser.getBooks();
                                        Collections.sort(books, (book1, book2) -> book1.getTitle().compareToIgnoreCase(book2.getTitle()));
                                        getView().safeExecute(view -> view.showBooks(books));
                                    },

                                    throwable -> {
                                        if (throwable != null && throwable.getMessage() != null) {
                                            log.logError("loadBooks Error: " + throwable.getMessage());
                                        }

                                        List<Book> bookListFromDB = null;

                                        if (ClassStatusManager.getInstance().getAppRunningMode() == AppRunningMode.OFFLINE) {

                                            bookListFromDB = bookSpecification.getDownloadedBooks();
                                        } else {
                                            bookListFromDB = bookshelfUser.getBooks();
                                        }

                                        if (BookListUtil.isFirstLoad()
                                                && bookListFromDB != null
                                                && bookListFromDB.size() > 0) {

                                            for (Book book : bookListFromDB) {
                                                if (book.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_ACTIVITY
                                                        || book.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_ACTIVITY) {

                                                    book.setGlobalDownloadStatus(DownloadStatus.PAUSED);
                                                    BookListUtil.setFirstLoad(false);
                                                    break;
                                                }
                                            }
                                        }

                                        if (throwable instanceof HttpException &&
                                                ((HttpException) throwable).code() == 403) {

                                            Response response = ((HttpException) throwable).response();

                                            JSONObject jObjError = null;
                                            try {
                                                jObjError = new JSONObject(response.errorBody().string());
                                                if (jObjError.has("Code")) {
                                                    String errorCode = jObjError.getString("Code");
                                                    if (errorCode.contentEquals(CustomErrorCodes.DISTRIBUIDOR_DEVICE_BLOCKED)) {
                                                        getView().safeExecute(BooksFragmentContract.View::onDeviceBlockedError);
                                                    }
                                                }
                                            } catch (JSONException | IOException e) {
                                                log.logError("loadBooks Error: " + throwable.getMessage());
                                            }
                                        }

                                        realm.close();

                                        List<Book> finalBookListFromDB = bookListFromDB;
                                        getView().safeExecute(view -> view.showBooks(finalBookListFromDB));
                                    },

                                    () -> log.logInfo("onCompleted")

                            ));
        } else {
            log.logError("DistribuidorService is null.");
            getView().safeExecute(view -> view.showBooks(null));
        }
    }

    @Override
    public void checkTemplateUpdate(TemplateService activityService,
                                    TemplateSpecification templateSpecification,
                                    RequestCreator requestCreator,
                                    Template newTemplate) {

        boolean useCache = false;

        if (templateSpecification != null && newTemplate != null) {
            templateSpecification.openRealm();
            Date currentDate = new Date();

            Template oldTemplate = templateSpecification.getLastTemplate();

            if (oldTemplate != null) {
                Date rangeDate = DateUtil.addSecondsToCurrentDate(oldTemplate.getLastUpdate(), oldTemplate.getExpiration());

                if (currentDate.before(rangeDate)) {
                    useCache = true;
                } else {
                    newTemplate.seteTag(oldTemplate.geteTag());
                }
            } else {
                newTemplate.seteTag("");
            }

            if (!useCache) {
                log.logInfo("Requesting template file");

                addSubscription(
                        activityService
                                .getActivity(newTemplate.geteTag())
                                .subscribeOn(Schedulers.io())
                                .observeOn(Schedulers.io())
                                .subscribe(response -> {
                                            newTemplate.seteTag(response.headers().get(Headers.ETag));
                                            newTemplate.setLastUpdate(currentDate);
                                            newTemplate.setExpiration(getMaxAge(response.headers()));

                                            if (response.code() == HttpURLConnection.HTTP_OK) {
                                                log.logInfo("checkTemplateUpdate - HTTP_OK");

                                                DownloadTemplateListener listener =
                                                        new DownloadTemplateListener(newTemplate, templateSpecification, downloadUnzip, log);

                                                DownloadRequest downloadRequest =
                                                        requestCreator.createNewDownloadRequest(
                                                                newTemplate.getDownloadPath(),
                                                                newTemplate.getZipPath(),
                                                                listener);

                                                requestCreator.getDownloadManager().add(downloadRequest);
                                            } else if (response.code() == HttpURLConnection.HTTP_NOT_MODIFIED) {
                                                log.logInfo("checkTemplateUpdate - HTTP_NOT_MODIFIED");
                                                templateSpecification.update(newTemplate);
                                            } else {
                                                log.logInfo("checkTemplateUpdate - ERROR");
                                            }
                                        },

                                        throwable -> {
                                            log.logError(throwable.getMessage());
                                        }));
            } else {
                log.logInfo("Using cache");
            }

            templateSpecification.closeRealm();
        }
    }

    private int getMaxAge(okhttp3.Headers headers) {
        int expiration = 0;

        List<String> values = headers.values(Headers.cacheControl);
        String maxAge = "";

        for (String value : values) {
            if (value.contains("max-age")) {
                maxAge = value;
            }
        }

        String[] maxAgeParts = maxAge.split("=");

        if (maxAgeParts != null && maxAgeParts.length > 1) {
            if (maxAgeParts[1] != null && !maxAgeParts[1].isEmpty()) {
                expiration = Integer.parseInt(maxAgeParts[1]);
            } else {
                log.logInfo("max-age is empty or null");
            }
        }

        return expiration;
    }

    public void searchByTitle(List<Book> bookList, String text) {
        List<Book> newFilteredBooks = new ArrayList<>();

        if (text != null && !text.isEmpty() && bookList != null) {
            addSubscription(Observable
                    .fromIterable(bookList)
                    .filter(book -> {
                        boolean contains = false;

                        String nQuery = Normalizer
                                .normalize(text.toLowerCase(), Normalizer.Form.NFD)
                                .replaceAll("[^\\p{ASCII}]", "");

                        if (book.getTitle() != null) {

                            String nTitle = Normalizer
                                    .normalize(book.getTitle().toLowerCase(), Normalizer.Form.NFD)
                                    .replaceAll("[^\\p{ASCII}]", "");

                            contains = nTitle.contains(nQuery);
                        }

                        if (!contains && book.getCollection() != null
                                && book.getCollection().getName() != null) {

                            String nCollection = Normalizer
                                    .normalize(book.getCollection().getName().toLowerCase(), Normalizer.Form.NFD)
                                    .replaceAll("[^\\p{ASCII}]", "");

                            contains = nCollection.contains(nQuery);
                        }

                        if (!contains && book.getPrimaryInfo() != null) {

                            String nPrimaryInfo = Normalizer
                                    .normalize(book.getPrimaryInfo().toLowerCase(), Normalizer.Form.NFD)
                                    .replaceAll("[^\\p{ASCII}]", "");

                            contains = nPrimaryInfo.contains(nQuery);
                        }

                        if (!contains && book.getSecondaryInfo() != null) {

                            String nSecondaryInfo = Normalizer
                                    .normalize(book.getSecondaryInfo().toLowerCase(), Normalizer.Form.NFD)
                                    .replaceAll("[^\\p{ASCII}]", "");

                            contains = nSecondaryInfo.contains(nQuery);
                        }

                        return contains;
                    })
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            filteredList -> {
                                newFilteredBooks.addAll(filteredList);
                                getView().safeExecute(view -> view.showFilterBooks(newFilteredBooks));
                            },

                            throwable -> {
                                if (throwable != null) {
                                    log.logError(throwable.getMessage());
                                }
                            })
            );
        } else {
            getView().safeExecute(view -> view.showFilterBooks(bookList));
        }
    }

    @Override
    public void searchBySubject(List<Book> bookList, String text) {
        List<Book> newFilteredBooks = new ArrayList<>();

        if (text != null && !text.isEmpty() && bookList != null) {
            addSubscription(Observable
                    .fromIterable(bookList)
                    .filter(book -> {
                                boolean result = false;

                                for (Subject subject : book.getSubjectList()) {
                                    if (subject.getExternalId() != null &&
                                            subject.getExternalId().toLowerCase().contentEquals(text.toLowerCase())) {
                                        result = true;
                                        break;
                                    }
                                }

                                return result;
                            }
                    )
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            filteredList -> {
                                newFilteredBooks.addAll(filteredList);
                                getView().safeExecute(view -> view.showFilterSubjects(newFilteredBooks));
                            },

                            throwable -> {
                                if (throwable != null) {
                                    log.logError(throwable.getMessage());
                                }
                            })
            );
        } else {
            List<Book> books = BookListUtil.getBookList();

            if (books != null) {
                newFilteredBooks.addAll(books);
            }

            getView().safeExecute(view -> view.showFilterSubjects(newFilteredBooks));
        }
    }
}