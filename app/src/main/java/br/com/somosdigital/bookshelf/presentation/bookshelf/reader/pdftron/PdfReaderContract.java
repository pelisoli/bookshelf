package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookFile;
import br.com.somosdigital.bookshelf.domain.utils.DrmClient;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by jgsmanaroulas on 20/01/17.
 */
public interface PdfReaderContract {

    String XFDF_TAG_ADD = "<add />";
    String XFDF_TAG_MODIFY = "<modify />";
    String XFDF_TAG_DELETE = "<delete />";

    String addName = "addAnnotations";
    String modifyName = "modfyAnnotations";
    String removeName = "removeAnnotations";

    interface View extends MvpView {

        void onLocalDocumentReady(int lastPage);

        void openFromUrl(String bookUrl, String password, int lastPage);

        void onActivitiesLoaded(List<BookActivityInfo> activitiesList);

        void showLoadingDocumentError();

        void onDocumentLoading();

        void onOedsLoaded(List<OedInfo> oeds);

        void onPasswordError();

        void onLoadActivitiesError();

        void onLoadOedsError();

        void onNoOedsLoaded();

        void importAnnot();

        void setHasError(boolean hasError);

        void setAnnotationErrorVisibility(boolean isVisible);

        void startSendingAnnotUpdate();

        void errorAnnotUpdate();

        void showAnnotationDownloadError();

        void clearBKPCommands();

        void closeReader();

        void clearCommandsLists();

        void setActivityMap(HashMap<Integer, List<BookActivityInfo>> activityMap);

        void loadBackupAnnots();

        void unsetFirstLoad();
    }

    interface Presenter {
        void loadPDFDocument(String bookUrl);

        void preloadActivities();

        void preloadOeds();

        String getBookFilePath(String id);

        String getDocumentPassword(DrmClient drmClient);

        void saveLastReadPage(int currentPage);

        void loadPageActivities(int[] visiblePages, HashMap<Integer, List<BookActivityInfo>> activityMap);

        void downloadXFDF();

        String readFileInPath(String filename);

        String getAnnotationsChanges();

        void sendAnnotaionUpdate(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands);

        void sendAnnotationsThenClose(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands);

        void registerAnnotation(String removeName, Map<String, String> annotations, String uuid, String xfdfCommand, String tagStart, String tagEnd);

        Map<String, String> getChangeList(String name);

        void discardAnnotationsBackup();

        void removeAnnotChangesFile();

        String getChangesFromBKP(Map<String, String> addCommandsBKP, Map<String, String> modifyCommandsBKP, Map<String, String> removeCommandsBKP);

        void sendBackupAnnotations(Map<String, String> addCommandsBKP, Map<String, String> modifyCommandsBKP, Map<String, String> removeCommandsBKP);
    }

    interface Interactor {

        Observable<BookFile> getBookFile();

        Observable<Response<ResponseBody>> downloadAnnotationFile();

        boolean checkIfDocumentExists();

        String getBookFilePath(String id);

        List<OedInfo> getOedList();

        void saveLastReadPage(int currentPage);

        String getDocumentPassword(DrmClient drmClient);

        Map<String, String> getChangeList(String name);

        void registerAnnotation(String name, Map<String, String> annotations, String uuid, String xfdfCommand, String tagStart, String tagEnd);

        String createXFDFString(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands);

        Observable<Response<ResponseBody>> sendAnnotations(String xfdfCommand);

        String getActivitiesFilePath();

        String readChangeFile();

        HashMap<Integer, List<BookActivityInfo>> getActivityMap() throws JSONException;

        List<BookActivityInfo> loadPageActivities(int[] visiblePages, HashMap<Integer, List<BookActivityInfo>> activityMap);

        int getLastReadPage();

        void removeAnnotChangesFile();

        void discarAnnotationsBackup();

    }
}
