package br.com.somosdigital.bookshelf.storage.repository.oed;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import io.realm.Realm;

/**
 * Created by pelisoli on 11/4/17.
 */

public class OedRepository implements OedSpecification {
    Realm realm;

    public OedRepository(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void add(Oed item) {
        if (checkRealmInstance(realm)) {
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<Oed> items) {
        if (checkRealmInstance(realm)) {
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(Oed item) {
        if (checkRealmInstance(realm)) {
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<Oed> items) {
        if (checkRealmInstance(realm)) {
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(Oed item) {
        if (checkRealmInstance(realm)) {
            realm.executeTransaction(realm1 -> item.deleteFromRealm());
        }
    }

    private boolean checkRealmInstance(Realm realm) {
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }
}