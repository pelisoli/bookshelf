package br.com.somosdigital.bookshelf.domain.upgrade;

import android.util.Log;

import java.io.File;

import br.com.somosdigital.bookshelf.domain.file.FileManager;
import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by marcus on 14/03/17.
 */

public class UpgradeUtil {

    public static Observable<Boolean> upgradeEpubToPdf(String basePath) {

        return Observable.defer(() -> {

            String bookDirectory = basePath + File.separator + "books";
            String pdfDirectory = basePath + File.separator + "books" + File.separator + "pdf";
            String toDelete = basePath + File.separator + "old";

            if (FileManager.directoryIsEmpty(pdfDirectory)) {

                Timber.i("Rename Book Directory");
                FileManager.renameDirectory(bookDirectory, toDelete);

                Timber.i("Create new Book Path");
                FileManager.createDirectory(pdfDirectory);

                Timber.i("Remove old Book Directory");
                FileManager.removeDirectory(toDelete);

            }

            return Observable.just(true);

        });

    }

    public static Observable<Boolean> removeAllBooks(String basePath) {
        return Observable.defer(() -> {
            String bookDirectory = basePath + File.separator + "books";
            String pdfDirectory = basePath + File.separator + "books" + File.separator + "pdf";
            String toDelete = basePath + File.separator + "old";

            //removes old directorory if already exists
            FileManager.removeDirectory(toDelete);

            Log.d("REMOVE BOOKS", "pasta existe, removendo");
            Log.d("REMOVE BOOKS", bookDirectory);
            //rename and remove book directory if exists
            FileManager.renameDirectory(bookDirectory, toDelete);
            FileManager.removeDirectory(toDelete);
//            throw Exceptions.propagate(new Throwable("erro de propósito"));
//
            Log.d("REMOVE BOOKS", "criando pasta");
            Log.d("REMOVE BOOKS", pdfDirectory);
            //finally recreate pdf directroy
            FileManager.createDirectory(pdfDirectory);

            return Observable.just(true);
        });
    }
}
