package br.com.somosdigital.bookshelf.domain.file;

import net.lingala.zip4j.core.ZipFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import timber.log.Timber;

/**
 * Created by pelisoli on 09/06/15.
 */
public class FileManager {

    public static void unzip(String filename, String directory, String password) throws Exception {
        if ((filename != null) && (directory != null)) {
            ZipFile zipFile = new ZipFile(filename);

            if (zipFile.isValidZipFile()) {
                if (password != null) {
                    zipFile.setPassword(password);
                }
                zipFile.extractAll(directory);
            } else {
                throw new IllegalStateException("Invalid zip file");
            }
        }
    }

    public static void unzip(String filename, String directory) throws Exception {
        unzip(filename, directory, null);
    }

    public static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    public static void deleteFile(String path){
        File file = new File(path);

        if(file.exists()){
            file.delete();
        }
    }

    public static void removeDirectory(String path){
        File directory = new File(path);

        if(directory.exists()){
            deleteDirectory(directory);
        }
    }

    public static void renameDirectory(String oldPath, String newPath){
        File oldDirectory = new File(oldPath);
        File newDirectory = new File(newPath);

        if(oldDirectory.exists() && !newDirectory.exists()){
            oldDirectory.renameTo(newDirectory);
        }
    }

    public static boolean directoryIsEmpty(String folder) {
        File directory = new File(folder);
        return !directory.exists() || directory.list().length == 0;
    }

    private static void deleteDirectory(File file){
        if(file.isDirectory()){

            //directory is empty, then delete it
            if(file.list().length==0){

                file.delete();

            }else{

                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);

                    //recursive delete
                    deleteDirectory(fileDelete);
                }

                //check the directory again, if empty then delete it
                if(file.list().length==0){
                    file.delete();
                }
            }

        }else{
            //if file, then delete it
            file.delete();
        }
    }


    public static boolean fileExists(String filePath){
        File file = new File(filePath);
        return file.exists();
    }

    public static boolean createDirectory(String folderPath){
        boolean status = false;

        if(folderPath != null || !folderPath.isEmpty()){
            File file = new File(folderPath);

            if(!file.exists()){
                if(file.mkdirs()){
                    status = true;
                }
            }else{
                status = true;
            }
        }

        return status;
    }

    public static boolean unzippingProcess(String tempBookPath, String compactedBookPath, String password){
        boolean status = false;

        Timber.i("Descompactação iniciada");

        try
        {
            if (new File(tempBookPath).exists()) {
                FileManager.removeDirectory(tempBookPath);
            }

            FileManager.unzip(compactedBookPath, tempBookPath, password);

            Timber.i("Descompactação finalizada");

            status = true;
        }catch (Exception e ){
            Timber.i("erro na descompactação");

            if(new File(tempBookPath).exists()) {
                FileManager.removeDirectory(tempBookPath);
            }

            if(new File(compactedBookPath).exists()){
                FileManager.removeDirectory(compactedBookPath);
            }
        }

        if (new File(compactedBookPath).delete()) {
            Timber.i("Arquivo compactado apagado com sucesso");
        } else {
            Timber.i("Erro ao apagar arquivo compactado");
        }

        return status;
    }
}
