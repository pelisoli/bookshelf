package br.com.somosdigital.bookshelf.domain.download.triggers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import br.com.converge.download.android.DownloadRequest;
import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadCallback;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookFile;
import br.com.somosdigital.bookshelf.domain.model.error.CustomErrorCodes;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadBook {

    private DownloadCallback downloadCallback;

    private DistribuidorService service;

    private DownloadStatusListener downloadStatusListener;

    private RequestCreator requestCreator;

    private CompositeDisposable compositeDisposable;

    private Log log;

    public DownloadBook(DownloadStatusListener downloadStatusListener,
                        DistribuidorService service,
                        DownloadCallback downloadCallback,
                        RequestCreator requestCreator,
                        CompositeDisposable compositeDisposable,
                        Log log){
        this.downloadStatusListener = downloadStatusListener;
        this.service = service;
        this.downloadCallback = downloadCallback;
        this.requestCreator = requestCreator;
        this.compositeDisposable = compositeDisposable;
        this.log = log;
    }

    public void prepareDownloadBook(Book book) {
        if(book != null) {
            BookFile postBookFile = new BookFile();
            postBookFile.setBookId(book.getId());
            postBookFile.setDevice(book.getDevice());
            postBookFile.setFileType(book.getFileType());
            postBookFile.setKeyCryptography(book.getKeyCryptography());


            compositeDisposable.add(
                    service.getBookFile(postBookFile)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    bookFile -> {
                                        book.setFilePath(bookFile.getFilePath());
                                        book.setKeyCryptography(bookFile.getKeyCryptography());

                                        if (!book.isUpdate()) {
                                            book.setEducationalObjects(bookFile.getEducationalObjects());
                                        }

                                        DownloadInformation.calculateDownloadSize(book, book.getDownloadType());
                                        downloadBook(book);
                                    },

                                    throwable -> {
                                        if(throwable != null && throwable.getMessage() != null) {
                                            log.logError("prepareDownloadBook Error: " + throwable.getMessage());
                                        }

                                        if (throwable instanceof HttpException &&
                                                ((HttpException) throwable).code() == 403) {

                                            Response response = ((HttpException)throwable).response();
                                            JSONObject jObjError = null;

                                            try {
                                                jObjError = new JSONObject(response.errorBody().string());
                                                if (jObjError.has("Code")){
                                                    String errorCode = jObjError.getString("Code");
                                                    if (errorCode.contentEquals(CustomErrorCodes.DISTRIBUIDOR_DEVICE_LIMIT_REACHED)){
                                                        downloadCallback.bookError(book, CustomErrorCodes.DISTRIBUIDOR_DEVICE_LIMIT_REACHED);
                                                    } else if (errorCode.contentEquals(CustomErrorCodes.DISTRIBUIDOR_DEVICE_BLOCKED)){
                                                        downloadCallback.bookError(book, CustomErrorCodes.DISTRIBUIDOR_DEVICE_BLOCKED);
                                                    }
                                                }
                                            } catch (JSONException | IOException e) {
                                                e.printStackTrace();
                                                log.logError("prepareDownloadBook Error: " + e.getMessage());
                                            }
                                        }else if (throwable instanceof HttpException &&
                                                ((HttpException) throwable).code() == 400){

                                            Response response = ((HttpException)throwable).response();
                                            JSONObject jObjError = null;

                                            try {
                                                jObjError = new JSONObject(response.errorBody().string());
                                                if (jObjError.has("Code")){
                                                    String errorCode = jObjError.getString("Code");

                                                    if (errorCode.contentEquals(CustomErrorCodes.BOOK_EXPIRED)) {
                                                        book.setExpiredDistribution(true);
                                                        downloadCallback.bookError(book, CustomErrorCodes.BOOK_EXPIRED);
                                                    }
                                                }
                                            } catch (JSONException | IOException e) {
                                                e.printStackTrace();
                                                log.logError("prepareDownloadBook Error: " + e.getMessage());
                                            }

                                        } else {
                                            downloadCallback.bookError(book, null);
                                        }
                                    }
                            )
            );
        }
    }

    private void downloadBook(Book book) {
        if (book != null) {
            DownloadRequest downloadRequest = requestCreator.createDownloadRequest(book.getFilePath(),
                    book.getCompactedPath(), downloadStatusListener);

            DownloadInformation.setCurrentDownloadId(requestCreator.getDownloadManager().add(downloadRequest));
        }
    }
}
