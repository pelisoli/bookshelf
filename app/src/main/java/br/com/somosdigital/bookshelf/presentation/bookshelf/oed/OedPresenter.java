package br.com.somosdigital.bookshelf.presentation.bookshelf.oed;

import java.io.File;

import br.com.solucaoadapta.shared.presentation.base.BasePresenter;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BooksValues;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderObject;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.utils.OedManagerAndroid;

/**
 * Created by marcus on 01/11/16.
 */
public class OedPresenter extends BasePresenter<OedContract.View>
        implements OedContract.Presenter {

    @Override
    public void loadOed(String objectPath, String oedId, String bookId, OedManagerAndroid oedManagerAndroid) {
        File file = new File(objectPath + "/index.html");
        if (file.exists()){
            if(oedManagerAndroid != null){
                String key = oedManagerAndroid.getKey(BooksValues.getInstance().getBookRootPath() + File.separator
                        + bookId + "/oeds/" + oedId + ".brk");
                getView().safeExecute(view -> view.onOedReadyLocally(objectPath, key));
            }
        } else {
            getView().safeExecute(OedContract.View::loadOedOnline);
        }
    }

    @Override
    public ReaderObject getObjectCallback(){
        return new ReaderObject() {
            @Override
            public void onObjectCalled(String oedID, String objectPath) {

            }

            @Override
            public void onActivityCalled(String activityId, String activityPath, String activityType) {

            }

            @Override
            public void onErrorToOpenObject(){

            }

            @Override
            public void onOpenMedia(String mediaPath) {
                getView().safeExecute(view -> view.openMedia(mediaPath));
            }
        };
    }
}
