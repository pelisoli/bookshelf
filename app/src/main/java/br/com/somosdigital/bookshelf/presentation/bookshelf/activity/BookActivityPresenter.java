package br.com.somosdigital.bookshelf.presentation.bookshelf.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.solucaoadapta.shared.BuildConfig;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.domain.services.aps.APSService;
import br.com.solucaoadapta.shared.presentation.base.BasePresenter;
import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.solucaoadapta.shared.storage.runtime.ClassStatusManager;
import br.com.somosdigital.bookshelf.domain.activity.AdaptaActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created by jgsmanaroulas on 11/1/16.
 */
public class BookActivityPresenter extends BasePresenter<BookActivityContract.View>
        implements BookActivityContract.Presenter {

    private Log log;

    private boolean isAuthorship;

    private boolean isTeacher;
    private boolean canSendActivty;

    private APSService apsService;

    public BookActivityPresenter(Log log, boolean isTeacher, boolean canSendActivty, APSService apsService){
        this.log = log;
        this.isTeacher = isTeacher;
        this.canSendActivty = canSendActivty;
        this.apsService = apsService;
    }

    @Override
    public void prepareActivity(AdaptaActivity adaptaActivity) {
        if(adaptaActivity != null) {
            if(adaptaActivity.getActivityInfo() != null){
                this.isAuthorship = adaptaActivity.getActivityInfo().isAutoria();
            }

            if (adaptaActivity.templateExists()) {
                if(adaptaActivity.activityExists()){
                    getView().safeExecute(view -> view.loadActivity(adaptaActivity.getLocalPath(isTeacher, canSendActivty)));
                }else{
                    getView().safeExecute(view -> view.loadActivity(adaptaActivity.getLocalTemplate(isTeacher, canSendActivty)));
                }
            } else {
                getView().safeExecute(view -> view.loadActivity(adaptaActivity.getWebPath(isTeacher, canSendActivty)));
            }
        }
    }


    @Override
    public void saveActivity(String statements, boolean keepOpen) {
        boolean showSaveStatementsMessage = true;
        boolean isDraft = false;
        JSONArray statementsJsonArray;
        JSONObject statementsPayload = new JSONObject();

        try {
            statementsJsonArray = new JSONArray(statements);
            statementsPayload.put("statements", statementsJsonArray);

            if (statementsJsonArray.length() > 0){
                JSONObject firstStatement = statementsJsonArray.getJSONObject(0);
                if (firstStatement.has("verb")){
                    String verb = firstStatement.getString("verb");
                    if (verb.contains("launched")){
                        showSaveStatementsMessage = false;
                    }
                }

                if (firstStatement.has("context")) {
                    JSONObject context = firstStatement.getJSONObject("context");
                    if (context.has("contextActivities")) {
                        JSONObject contextActivities = context.getJSONObject("contextActivities");
                        JSONArray parent = contextActivities.getJSONArray("parent");
                        if (parent.length() > 0) {
                            if (parent.getJSONObject(0).has("id")) {
                                String fullId = parent.getJSONObject(0).getString("id");
                            }
                        }
                    } else {
                        if (firstStatement.has("object")) {
                            JSONObject object = firstStatement.getJSONObject("object");
                            if (object.has("id")) {
                                String fullId = object.getString("id");
                            }
                        }
                    }

                    if(context.has("extensions")){
                        JSONObject extensions = context.getJSONObject("extensions");
                        if(extensions.has("http://solucaoadapta/draft")){
                            isDraft = extensions.getBoolean("http://solucaoadapta/draft");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            log.logError(e.getMessage());
        }

        boolean finalShowSaveStatementsMessage = showSaveStatementsMessage;
        boolean finalIsDraft = isDraft;

        addSubscription(apsService.saveStatements(statementsPayload)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            if (response != null) {
                                if (finalIsDraft){
                                    getView().safeExecute(BookActivityContract.View::showSendViewSuccess);
                                }
                            } else {
                                if (finalShowSaveStatementsMessage) {
                                    getView().safeExecute(BookActivityContract.View::showSendError);
                                }
                            }
                        },

                        throwable -> {
                            log.logError("saveActivity: " + throwable.getMessage());

                            if (throwable instanceof HttpException &&
                                    (((HttpException) throwable).code() == 503 || ((HttpException) throwable).code() == 502)) {

                                getView().safeExecute(MvpView::showServerMaintenanceError);
                            } else {

                                if (finalShowSaveStatementsMessage) {
                                    getView().safeExecute(BookActivityContract.View::showSendError);
                                }
                            }
                        }
                ));
    }

    @Override
    public String getClassInfo(Object object, AdaptaActivity adaptaActivity, String token, String userId, boolean isTeacher) {
        JSONObject classInfo = new JSONObject();
        JSONObject agentInfo = new JSONObject();

        try {

            String userName = "";
            if (ClassStatusManager.getInstance().getCurrentUser() != null){
                userName = ClassStatusManager.getInstance().getCurrentUser().getName();
            }

            String groupId = "";
            if (ClassStatusManager.getInstance().getCurrentGroup() != null){
                groupId = ClassStatusManager.getInstance().getCurrentGroup().getId();
            }

            String subjectId = "";
            if (ClassStatusManager.getInstance().getCurrentSubject() != null){
                subjectId = ClassStatusManager.getInstance().getCurrentSubject().getId();
            }

            agentInfo.put("name", userId);
            classInfo.put("apsUrlVersion", BuildConfig.APS_VERSION);
            classInfo.put("apsUri", BuildConfig.APS_URL);
            classInfo.put("agent", agentInfo);
            classInfo.put("accessToken", "Bearer " + token);
            //

            classInfo.put("lrsUriVerb", BuildConfig.LRS_VERB);
            classInfo.put("isTeacher", isTeacher);
            classInfo.put("userName", userName);
            classInfo.put("sentDate", "");
            classInfo.put("groupId", groupId);
            classInfo.put("subjectId", subjectId);

            classInfo.put("activityResourcesPath", adaptaActivity.getActivityResourcesPath());

            if(adaptaActivity.templateExists()){
                if(adaptaActivity.activityExists()) {
                    classInfo.put("activityPath", adaptaActivity.getActivityJsonPath());
                }
            }

            String apsUrl = BuildConfig.APS_URL;

            if(apsUrl.length() > 4){
                apsUrl = apsUrl.substring(0, apsUrl.length() - 4);
                classInfo.put("resourceServerUrl", apsUrl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return classInfo.toString();
    }
}
