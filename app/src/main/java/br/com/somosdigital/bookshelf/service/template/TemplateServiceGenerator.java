package br.com.somosdigital.bookshelf.service.template;

import android.content.Context;

import br.com.somosdigital.bookshelf.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import timber.log.Timber;

/**
 * Created by pelisoli on 09/02/17.
 */

public class TemplateServiceGenerator {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

    public static TemplateInterface createService(String apiBaseUrl, Context context) {
        builder.baseUrl(apiBaseUrl);

        loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.tag("OkHttp").d(message));
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggingInterceptor);

        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(TemplateInterface.class);

    }

    public static TemplateInterface createService(Context context) {
        return createService(BuildConfig.TEMPLATE_HTTP, context);

    }

}
