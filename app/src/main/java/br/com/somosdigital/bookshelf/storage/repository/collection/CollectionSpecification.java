package br.com.somosdigital.bookshelf.storage.repository.collection;


import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Collection;

/**
 * Created by pelisoli on 16/09/16.
 */
public interface CollectionSpecification extends Repository<Collection> {
}
