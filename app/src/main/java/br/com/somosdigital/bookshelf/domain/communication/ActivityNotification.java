package br.com.somosdigital.bookshelf.domain.communication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import timber.log.Timber;

/**
 * Created by jgsmanaroulas on 19/12/16.
 */
public class ActivityNotification extends BaseNotification {
    private static final String ACTIVITY = "activityId";

    public static JSONObject sendActivity(int protocolId, List<String> userIds){
        return build(protocolId, userIds);
    }

    public static JSONObject sendActivityTry(int protocolId, List<String> userIds, String activityId){
        JSONObject message = build(protocolId, userIds);

        if(message != null){
            try {
                message.put(ACTIVITY, activityId);
            } catch (JSONException e) {
                Timber.e(e.getMessage());
            }
        }

        return message;
    }

}
